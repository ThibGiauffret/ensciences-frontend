// Librairies
import { useEffect, useState } from 'react';
import { useToasts } from 'react-bootstrap-toasts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas);

// Components
import Container from 'react-bootstrap/Container';
import { LoginCard } from '../components/cards';
import { Row, Col, Form, InputGroup, Button } from 'react-bootstrap';

// CSS
import styles from './css/login.module.css';

// Functions
import login from '../functions/users/login';
import register from '../functions/users/register';
import removeParam from '../functions/url/removeParam';
import logout from '../functions/users/logout';

function Login(props: { title: string }) {
    // Set the title of the page
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title;
        };
        setTitle();
    }, []);

    // Initialize the toasts
    const toasts = useToasts();

    // Handle logout
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('logout')) {
        removeParam(window.location.href, ['logout']);
        // Remove wordpress cookie
        logout().then((response) => {
            if (response.status === 'success') {
                // Redirect to the home page
                toasts.show({
                    headerContent: 'Déconnexion',
                    bodyContent: 'Vous avez été déconnecté.',
                    toastProps: {
                        bg: 'success',
                        autohide: true,
                        delay: 3000,
                    },
                });
            } else {
                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: 'Erreur lors de la déconnexion.',
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 3000,
                    },
                });
            }            
        });
    }

    // Handle login form
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = async (event: React.FormEvent) => {
        event.preventDefault();
        login(email, password).then((response) => {
            if (response.status === 'success') {
                // Redirect to the home page
                window.location.href = '/?notification=success&message=Connexion%20réussie.';
            } else {
                let errorMessage = "";
                if (response.status === 'missing_fields') {
                    // Highlight the missing fields
                    if (!email) {
                        document.getElementById("emailLogin")?.classList.add("is-invalid");
                    }
                    if (!password) {
                        document.getElementById("passwordLogin")?.classList.add("is-invalid");
                    }
                    errorMessage = "Veuillez remplir tous les champs.";
                } else if (response.status === 'invalid_email') {
                    // Highlight the invalid email
                    document.getElementById("emailLogin")?.classList.add("is-invalid");
                    errorMessage = "L'adresse email n'est pas valide.";
                } else if (response.status === 'bad_credentials') {
                    // Highlight the invalid email
                    document.getElementById("emailLogin")?.classList.add("is-invalid");
                    document.getElementById("passwordLogin")?.classList.add("is-invalid");
                    errorMessage = "Email ou mot de passe incorrect.";
                } else {
                    errorMessage = "Erreur lors de la connexion.";
                }


                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: errorMessage,
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 3000,
                    },
                });
            }
        });

    };

    // Handle register form
    const [nameRegister, setNameRegister] = useState('');
    const [surnameRegister, setSurnameRegister] = useState('');
    const [usernameRegister, setUsernameRegister] = useState('');
    const [emailRegister, setEmailRegister] = useState('');
    const [passwordRegister, setPasswordRegister] = useState('');
    const [passwordConfirmationRegister, setPasswordConfirmationRegister] = useState('');
    const [confirmValidity, setConfirmValidity] = useState(false);
    const [passwordStrength, setPasswordStrength] = useState<string[]>([]);

    const handleRegister = (event: React.FormEvent) => {
        event.preventDefault();
        register(nameRegister, surnameRegister, usernameRegister, emailRegister, passwordRegister, passwordConfirmationRegister).then((response) => {
            if (response) {
                if (response.status === "success") {
                    // Show a success toast
                    toasts.show({
                        headerContent: 'Succès',
                        bodyContent: 'Un email de confirmation vous a été envoyé. Veuillez vérifier votre boîte mail.',
                        toastProps: {
                            bg: 'success',
                            autohide: true,
                            delay: 5000,
                        },
                    });

                    // Clear all register fields
                    clearFields();

                    // Focus on the login tab
                    const loginTabButton = document.getElementById('login-tab');
                    loginTabButton?.click();

                } else {

                    let errorMessage = "";

                    // Clear all invalid fields
                    clearInvalidFields();

                    // Build the error message and highlight the invalid fields
                    if (response.errors.includes('missing_fields')) {
                        errorMessage += " Veuillez remplir tous les champs.";
                        // Highlight the missing fields
                        if (!nameRegister) {
                            document.getElementById("nameRegister")?.classList.add("is-invalid");
                        }
                        if (!surnameRegister) {
                            document.getElementById("surnameRegister")?.classList.add("is-invalid");
                        }
                        if (!usernameRegister) {
                            document.getElementById("usernameRegister")?.classList.add("is-invalid");
                        }
                        if (!emailRegister) {
                            document.getElementById("emailRegister")?.classList.add("is-invalid");
                        }
                        if (!passwordRegister) {
                            document.getElementById("passwordRegister")?.classList.add("is-invalid");
                        }
                        if (!passwordConfirmationRegister) {
                            document.getElementById("passwordConfirmationRegister")?.classList.add("is-invalid");
                        }
                    }
                    if (response.errors.includes("passwords_do_not_match")) {
                        errorMessage += "Les mots de passe ne correspondent pas.";
                        document.getElementById("passwordConfirmationRegister")?.classList.add("is-invalid");
                    }
                    if (response.errors.includes('username_already_taken')) {
                        errorMessage += " Un compte utilise déjà ce nom d'utilisateur.";
                        document.getElementById("usernameRegister")?.classList.add("is-invalid");
                    }
                    if (response.errors.includes('email_already_taken')) {
                        errorMessage += " Un compte utilise déjà cet email.";
                        document.getElementById("emailRegister")?.classList.add("is-invalid");
                    }
                    if (response.errors.includes('password_not_strong_enough')) {
                        errorMessage += " Le mot de passe n'est pas assez fort.";
                        document.getElementById("passwordRegister")?.classList.add("is-invalid");
                    }
                    if (response.errors.includes('invalid_email')) {
                        errorMessage += " L'adresse email n'est pas valide.";
                        document.getElementById("emailRegister")?.classList.add("is-invalid");
                    }

                    // Show an error toast
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: errorMessage,
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 5000,
                        },
                    });
                }
            }
        }
        );
    };


    return (
        <Container id="mainLogin" className={styles.mainLogin}>
            <Button variant="primary" href="/">
                <FontAwesomeIcon icon="arrow-left" /> Retourner sur EnSciences
            </Button>

            {/* EnSciences logo centered */}
            <div className={styles.logoContainer}>
                <img src="/favicon_color.svg" alt="EnSciences" className={styles.logo} />
            </div>

            {/* Login form in a card with two tabs */}
            <LoginCard className={styles.loginCard}>
                <form id="loginForm" onSubmit={handleLogin}>
                    {/* Email */}
                    <InputGroup className="mb-3">
                        <InputGroup.Text id="emailGroup">
                            <FontAwesomeIcon icon="envelope" />
                        </InputGroup.Text>
                        <Form.Control
                            placeholder="Email"
                            aria-label="Email"
                            aria-describedby="emailGroup"
                            id="emailLogin"
                            name="emailLogin"
                            onChange={(e) => {
                                document.getElementById("emailLogin")?.classList.remove("is-invalid");
                                setEmail(e.target.value)
                            }
                            }
                        />
                    </InputGroup>

                    {/* Password */}
                    <InputGroup>
                        <InputGroup.Text id="passwordGroup">
                            <FontAwesomeIcon icon="lock" />
                        </InputGroup.Text>
                        <Form.Control
                            type="password"
                            placeholder="Mot de passe"
                            aria-label="Mot de passe"
                            aria-describedby="passwordGroup"
                            id="passwordLogin"
                            name="passwordLogin"
                            onChange={(e) => {
                                document.getElementById("passwordLogin")?.classList.remove("is-invalid");
                                setPassword(e.target.value)
                            }
                            }
                        />
                    </InputGroup>

                    {/* Forgotten */}
                    <a href="#" className="small">Mot de passe oublié ?</a>

                    {/* Remember */}
                    <Form.Check
                        type="checkbox"
                        id="rememberCheck"
                        label="Se souvenir de moi"
                        className='mt-3'
                    />

                    {/* Submit */}
                    <button type="submit" className="btn btn-success mx-auto mt-3 w-100" id="loginButton" name="loginButton">
                        <FontAwesomeIcon icon="sign-in-alt" />&nbsp;Connexion
                    </button>
                </form>
                <form id="registerForm" onSubmit={handleRegister}>
                    {/* Name and surname */}
                    <Row className="mb-3">
                        <Col>
                            <Form.Control
                                placeholder="Nom"
                                aria-label="Nom"
                                id="nameRegister"
                                name="nameRegister"
                                onChange={(e) => {
                                    setNameRegister(e.target.value)
                                    document.getElementById("nameRegister")?.classList.remove("is-invalid");
                                }}
                            />
                        </Col>
                        <Col>
                            <Form.Control
                                placeholder="Prénom"
                                aria-label="Prénom"
                                id="surnameRegister"
                                name="surnameRegister"
                                onChange={(e) => {
                                    setSurnameRegister(e.target.value)
                                    document.getElementById("surnameRegister")?.classList.remove("is-invalid");
                                }}
                            />
                        </Col>
                    </Row>

                    {/* Username */}
                    <InputGroup className="mb-3">
                        <InputGroup.Text id="usernameGroup">
                            <FontAwesomeIcon icon="user" />
                        </InputGroup.Text>
                        <Form.Control
                            placeholder="Nom d'utilisateur"
                            aria-label="Nom d'utilisateur"
                            aria-describedby="usernameGroup"
                            id="usernameRegister"
                            name="usernameRegister"
                            onChange={(e) => {
                                setUsernameRegister(e.target.value)
                                document.getElementById("usernameRegister")?.classList.remove("is-invalid");
                            }}
                        />
                    </InputGroup>

                    {/* Email */}
                    <InputGroup className="mb-3">
                        <InputGroup.Text id="emailGroup">
                            <FontAwesomeIcon icon="envelope" />
                        </InputGroup.Text>
                        <Form.Control
                            placeholder="Email"
                            aria-label="Email"
                            aria-describedby="emailGroup"
                            id="emailRegister"
                            name="emailRegister"
                            onChange={(e) => {
                                setEmailRegister(e.target.value)
                                document.getElementById("emailRegister")?.classList.remove("is-invalid");
                            }}
                        />
                    </InputGroup>

                    {/* Password */}
                    <Row>
                        <Col>
                            <InputGroup>
                                <InputGroup.Text id="passwordGroup">
                                    <FontAwesomeIcon icon="lock" />
                                </InputGroup.Text>
                                <Form.Control
                                    type="password"
                                    placeholder="Mot de passe"
                                    aria-label="Mot de passe"
                                    aria-describedby="passwordGroup"
                                    id="passwordRegister"
                                    name="passwordRegister"
                                    className={passwordRegister ? (passwordStrength.length < 5 ? "is-invalid" : "") : ""}
                                    onChange={(e) => {
                                        setPasswordRegister(e.target.value)

                                        if (e.target.value !== passwordConfirmationRegister && passwordConfirmationRegister !== "") {
                                            setConfirmValidity(false);
                                        } else {
                                            setConfirmValidity(true);
                                        }

                                        // Check password strength (at least 8 characters, 1 uppercase, 1 lowercase, 1 number and 1 special character)
                                        let strength = []
                                        if (e.target.value.length >= 8) {
                                            strength.push("length")
                                        }
                                        if (e.target.value.match(/[A-Z]/)) {
                                            strength.push("uppercase")
                                        }
                                        if (e.target.value.match(/[a-z]/)) {
                                            strength.push("lowercase")
                                        }
                                        if (e.target.value.match(/[0-9]/)) {
                                            strength.push("number")
                                        }
                                        if (e.target.value.match(/[!@#$%^&*]/)) {
                                            strength.push("special")
                                        }
                                        setPasswordStrength(strength);
                                    }}
                                />
                            </InputGroup>
                        </Col>
                        <Col>
                            {/* Password confirm */}
                            <Form.Control
                                type="password"
                                placeholder="Confirmer le mot de passe"
                                aria-label="Confirmer le mot de passe"
                                id="passwordConfirmationRegister"
                                name="passwordConfirmationRegister"
                                className={passwordConfirmationRegister ? (!confirmValidity ? "is-invalid" : "") : ""}
                                onChange={(e) => {
                                    setPasswordConfirmationRegister(e.target.value)

                                    if (e.target.value !== passwordRegister) {
                                        setConfirmValidity(false);
                                    } else {
                                        setConfirmValidity(true);
                                    }
                                }}
                            />
                        </Col>
                    </Row>


                    {/* Password strength */}
                    <Row className="mt-2">
                        <Col>
                            <ul className="small">
                                <li className={passwordRegister && passwordStrength.includes("length") ? styles.validText : passwordRegister ? styles.invalidText : ""}>8 caractères minimum</li>
                                <li className={passwordRegister && passwordStrength.includes("uppercase") ? styles.validText : passwordRegister ? styles.invalidText : ""}>1 majuscule</li>
                                <li className={passwordRegister && passwordStrength.includes("lowercase") ? styles.validText : passwordRegister ? styles.invalidText : ""}>1 minuscule</li>
                            </ul>
                        </Col>
                        <Col>
                            <ul className="small">
                                <li className={passwordRegister && passwordStrength.includes("number") ? styles.validText : passwordRegister ? styles.invalidText : ""}>1 chiffre</li>
                                <li className={passwordRegister && passwordStrength.includes("special") ? styles.validText : passwordRegister ? styles.invalidText : ""}>1 caractère spécial (!@#$%^&*)</li>
                                <li className={passwordConfirmationRegister && confirmValidity ? styles.validText : passwordConfirmationRegister ? styles.invalidText : ""}>Les mots de passe correspondent</li>
                            </ul>
                        </Col>
                    </Row>

                    {/* Submit */}
                    <button type="submit" className="btn btn-success mx-auto mt-3 w-100" id="registerButton" name="registerButton">
                        <FontAwesomeIcon icon="user-plus" />&nbsp;Inscription
                    </button>
                </form>
            </LoginCard>

        </Container>
    );

    // Remove invalid fields classes
    function clearInvalidFields() {
        document.getElementById("nameRegister")?.classList.remove("is-invalid");
        document.getElementById("surnameRegister")?.classList.remove("is-invalid");
        document.getElementById("usernameRegister")?.classList.remove("is-invalid");
        document.getElementById("emailRegister")?.classList.remove("is-invalid");
        document.getElementById("passwordRegister")?.classList.remove("is-invalid");
        document.getElementById("passwordConfirmationRegister")?.classList.remove("is-invalid");
        document.getElementById("passwordConfirmationRegister")?.classList.remove("is-valid");
        document.getElementById("passwordRegister")?.classList.remove("is-valid");
    }

    // Clear all register fields
    function clearFields() {
        clearInvalidFields();
        clearInputValueById("nameRegister");
        clearInputValueById("surnameRegister");
        clearInputValueById("usernameRegister");
        clearInputValueById("emailRegister");
        clearInputValueById("passwordRegister");
        clearInputValueById("passwordConfirmationRegister");
        setNameRegister("");
        setSurnameRegister("");
        setUsernameRegister("");
        setEmailRegister("");
        setPasswordRegister("");
        setPasswordConfirmationRegister("");
        setConfirmValidity(false);
        setPasswordStrength([]);
    }

    // Clear input value by id
    function clearInputValueById(id: string) {
        const element = document.getElementById(id) as HTMLInputElement;
        if (element) {
            element.value = "";
        }
    };

}


// Export
export default Login;