// Components
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import InputGroup from 'react-bootstrap/InputGroup';
import { CollapsibleAlert } from '../collapsibleAlert';
import IconSelector from '../iconSelector';
import { Button } from 'react-bootstrap';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

// CSS
import styles from '../css/cards.module.css';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useEffect, useState, useRef } from 'react';

// Themes editor card (modal content)
function ThemesEditorContent({ version, json, setJson, setModified }: { version: string, json: Lessons, setJson: any, setModified: any }) {

    const isFirstRender = useRef(true);

    // Filter the json.version and keep only the version we want to edit
    const versionData = json.versions.filter((v: Version) => v.version === version);
    const [themes, setThemes] = useState<Theme[]>(versionData[0].themes);

    // Send the updated themes to the parent component when the themes are updated
    useEffect(() => {
        // Do nothing on the first render
        if (isFirstRender.current) {
            isFirstRender.current = false;
            return;
        }

        setJson({
            ...json, versions: json.versions.map((v: Version) => {
                if (v.version === version) {
                    return { ...v, themes: themes };
                }
                return v;
            })
        });
        setModified(true);
    }, [themes]);

    // Handle the tab click
    const [activeTab, setActiveTab] = useState<number>(themes.length > 0 ? themes[0].id : 0);
    const handleTabClick = (tabId: number) => {
        setActiveTab(tabId);
    };

    // Add a new theme
    const handleAddTheme = () => {
        const newThemes = addTheme([...themes]);
        let newThemeId = 0;
        if (newThemes.length > 0) {
            newThemeId = newThemes[newThemes.length - 1].id;
        }
        setThemes(newThemes);
        setActiveTab(newThemeId);
    };

    return (
        <Card className={styles.card + " mb-3"} key={themes.length}>
            <Card.Header>
                <ul className="nav nav-tabs card-header-tabs">
                    {/* Theme ids */}
                    {themes && themes.map((theme: Theme) => (
                        <li key={theme.id + '_theme_nav'} className="nav-item">
                            <a onClick={() => {
                                handleTabClick(theme.id)
                            }} className={"nav-link " + (
                                activeTab === theme.id ? 'active' : ''
                            )}>
                                {theme.name}
                            </a>
                        </li>
                    ))}

                    {/* Add theme button */}
                    <li className="nav-item">
                        <a className='nav-link' onClick={handleAddTheme}>
                            <FontAwesomeIcon icon="plus" />
                        </a>
                    </li>
                </ul>
            </Card.Header>

            {/* Card Body */}
            <Card.Body>
                <div className="tab-content">
                    {themes && themes.map((theme: Theme) => (
                        <div key={theme.id} className={"tab-pane " + (
                            activeTab === theme.id ? 'active' : ''
                        )} id={theme.id.toString()}>

                            <div key={theme.id + "_theme_content"}>
                                <h4>Paramétrage du thème</h4>
                                {/* Theme parameters */}
                                <InputGroup className="mb-3" size="sm">
                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Identifiant unique du thème.</Tooltip>}>
                                        <InputGroup.Text id="theme_id">
                                            #
                                        </InputGroup.Text>
                                    </OverlayTrigger>
                                    <Form.Control
                                        placeholder="Identifiant"
                                        aria-label="Identifiant"
                                        aria-describedby="theme_id"
                                        value={theme.name}
                                        data-path={"themes[" + theme.id + "].name"}
                                        onChange={(e) => {
                                            const updatedThemes = themes.map((t: Theme) => {
                                                if (t.id === theme.id) {
                                                    return { ...t, name: e.target.value };
                                                }
                                                return t;
                                            });
                                            setThemes(updatedThemes);
                                        }}
                                    />
                                    {/* Delete theme button */}
                                    <Button variant="danger" size="sm" onClick={() => {
                                        if (confirm("Voulez-vous vraiment supprimer ce thème ?")) {
                                            const updatedThemes = themes.filter((t: Theme) => t.id !== theme.id);
                                            setThemes(updatedThemes);
                                            setActiveTab(updatedThemes[updatedThemes.length - 1].id);
                                        }
                                    }}>
                                        <FontAwesomeIcon icon="trash" />
                                    </Button>
                                </InputGroup>

                                <InputGroup className="mb-3" size="sm">
                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Titre du thème.</Tooltip>}>
                                        <InputGroup.Text id="theme_name">
                                            <FontAwesomeIcon icon="font" />
                                        </InputGroup.Text>
                                    </OverlayTrigger>
                                    <Form.Control
                                        placeholder="Titre du chapitre"
                                        aria-label="Titre du chapitre"
                                        aria-describedby="theme_name"
                                        value={theme.title}
                                        data-path={"themes[" + theme.id + "].title"}
                                        onChange={(e) => {
                                            const updatedThemes = themes.map((t: Theme) => {
                                                if (t.id === theme.id) {
                                                    return { ...t, title: e.target.value };
                                                }
                                                return t;
                                            });
                                            setThemes(updatedThemes);
                                        }}
                                    />
                                </InputGroup>
                                <Row xs={2}>
                                    <Col>
                                        <InputGroup className="mb-3" size="sm">
                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Icône du thème.</Tooltip>}>
                                                <InputGroup.Text id="theme_icon">
                                                    <FontAwesomeIcon icon="star" />
                                                </InputGroup.Text>
                                            </OverlayTrigger>
                                            <IconSelector value={theme.icon} setValue={(icon: string) => {
                                                const updatedThemes = themes.map((t: Theme) => {
                                                    if (t.id === theme.id) {
                                                        return { ...t, icon: icon };
                                                    }
                                                    return t;
                                                }
                                                );
                                                setThemes(updatedThemes);
                                            }
                                            } />
                                        </InputGroup>
                                    </Col>
                                    <Col>
                                        <InputGroup className="mb-3" size="sm">
                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Couleur du thème.</Tooltip>}>
                                                <InputGroup.Text id="theme_color">
                                                    <FontAwesomeIcon icon="palette" />
                                                </InputGroup.Text>
                                            </OverlayTrigger>
                                            <Form.Control
                                                size="sm"
                                                type="color"
                                                value={theme.color}
                                                aria-label="Couleur"
                                                aria-describedby="theme_color"
                                                data-path={"themes[" + theme.id + "].color"}
                                                onChange={(e) => {
                                                    const updatedThemes = themes.map((t: Theme) => {
                                                        if (t.id === theme.id) {
                                                            return { ...t, color: e.target.value };
                                                        }
                                                        return t;
                                                    });
                                                    setThemes(updatedThemes);
                                                }}
                                            />
                                        </InputGroup>
                                    </Col>
                                </Row>

                                <hr />

                                <h4>Chapitres</h4>

                                {/* Chapter list */}
                                {theme.chapters && theme.chapters.map((chapter: Chapter) => (
                                    <>
                                        <CollapsibleAlert key={"theme_" + theme.id + "_chapter_" + chapter.id} title={chapter.name} variant="secondary">
                                            <div className="mt-3">
                                                {/* Id */}
                                                <InputGroup size="sm" className="mb-3">
                                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Identifiant unique du chapitre qui constitue le début du nom des fichiers associés.</Tooltip>}>
                                                        <InputGroup.Text id="chapter_id">
                                                            #
                                                        </InputGroup.Text>
                                                    </OverlayTrigger>
                                                    <Form.Control
                                                        placeholder="Identifiant"
                                                        aria-label="Identifiant"
                                                        aria-describedby="chapter_id"
                                                        value={chapter.name}
                                                        data-path={"themes[" + theme.id + "].chapters[" + chapter.id + "].name"}
                                                        onChange={(e) => {
                                                            const updatedThemes = themes.map((t: Theme) => {
                                                                if (t.id === theme.id) {
                                                                    const updatedChapters = t.chapters.map((c: Chapter) => {
                                                                        if (c.id === chapter.id) {
                                                                            return { ...c, name: e.target.value };
                                                                        }
                                                                        return c;
                                                                    });
                                                                    return { ...t, chapters: updatedChapters };
                                                                }
                                                                return t;
                                                            });
                                                            setThemes(updatedThemes);
                                                        }}
                                                    />
                                                    <Button variant="danger" size="sm" onClick={() => {
                                                        if (confirm("Voulez-vous vraiment supprimer ce chapitre ?")) {
                                                            const updatedThemes = themes.map((t: Theme) => {
                                                                if (t.id === theme.id) {
                                                                    const updatedChapters = t.chapters.filter((c: Chapter) => c.id !== chapter.id);
                                                                    return { ...t, chapters: updatedChapters };
                                                                }
                                                                return t;
                                                            });
                                                            setThemes(updatedThemes);
                                                        }
                                                    }}>
                                                        <FontAwesomeIcon icon="trash" />
                                                    </Button>

                                                </InputGroup>

                                                {/* Title */}
                                                <InputGroup size="sm">
                                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Titre du chapitre.</Tooltip>}>
                                                        <InputGroup.Text id="chapter_name">
                                                            <FontAwesomeIcon icon="font" />
                                                        </InputGroup.Text>
                                                    </OverlayTrigger>
                                                    <Form.Control
                                                        placeholder="Titre du chapitre"
                                                        aria-label="Titre du chapitre"
                                                        aria-describedby="chapter_name"
                                                        value={chapter.title}
                                                        data-path={"themes[" + theme.id + "].chapters[" + chapter.id + "].title"}
                                                        onChange={(e) => {
                                                            const updatedThemes = themes.map((t: Theme) => {
                                                                if (t.id === theme.id) {
                                                                    const updatedChapters = t.chapters.map((c: Chapter) => {
                                                                        if (c.id === chapter.id) {
                                                                            return { ...c, title: e.target.value };
                                                                        }
                                                                        return c;
                                                                    });
                                                                    return { ...t, chapters: updatedChapters };
                                                                }
                                                                return t;
                                                            });
                                                            setThemes(updatedThemes);
                                                        }}
                                                    />
                                                </InputGroup>
                                            </div>
                                        </CollapsibleAlert>
                                    </>
                                ))}

                                {/* Add chapter button */}
                                <Button variant="success" size="sm" className="mt-3" onClick={() => {
                                    var newThemes = addChapter(theme.id, themes);
                                    const newThemesCopy = [...newThemes];
                                    setThemes(newThemesCopy);
                                }}>
                                    <FontAwesomeIcon icon="plus" />&nbsp;Ajouter un chapitre
                                </Button>
                            </div>
                        </div>
                    ))}
                </div>

            </Card.Body>
        </Card>
    )
}


// Add a new theme to the themes list
function addTheme(themes: Theme[]) {
    themes.push({
        id: themes.length,
        name: "theme",
        title: "",
        icon: "",
        color: "",
        chapters: []
    });

    return themes;
}

// Add a new chapter to the chapters list
function addChapter(themeId: Number, themes: Theme[]) {
    themes.map((theme: Theme) => {
        if (theme.id === themeId) {
            theme.chapters.push({
                id: theme.chapters.length,
                name: "",
                title: "",
                files: []
            });
        }
    })
    return themes;
}

export default ThemesEditorContent;