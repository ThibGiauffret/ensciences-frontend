function getTheme() {
    return localStorage.getItem('theme') || 'light';
}

function setTheme(theme: string) {
    const htmlElement = document.querySelector('html');
    if (htmlElement) {
        htmlElement.setAttribute('data-bs-theme', theme);
    }
}


function switchTheme() {
    // Add data-bs-theme attribute to the html element
    const htmlElement = document.querySelector('html');
    if (htmlElement) {
        if (htmlElement.getAttribute('data-bs-theme') === 'dark') {
            setTheme('light');
        } else {
            setTheme('dark');
        }
    }
    // Store the theme in the local storage
    const theme = htmlElement?.getAttribute('data-bs-theme');
    if (theme) {
        localStorage.setItem('theme', theme);
    }
}

export { getTheme, setTheme, switchTheme };