async function uploadFile(level: string, version: string, folder: string, file: File) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_BACKEND_URL;

    // Create FormData
    const formData = new FormData();
    formData.append('level', level);
    formData.append('version', version);
    formData.append('folder', folder);
    formData.append('file', file);

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'api/uploadFile.php', {
            method: 'POST',
            body: formData,
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            return { "status": "success", "message": data.message };
        } else {
            return { 'status': data.status, 'message': data.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { uploadFile };