// Components to build the lessons page with themes, chapters and files

// Imports
import { useEffect, useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Tooltip from 'react-bootstrap/Tooltip';
import Popover from 'react-bootstrap/Popover';
import Table from 'react-bootstrap/Table';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import { Form, InputGroup, Dropdown, Placeholder } from 'react-bootstrap';
import { CollapsibleAlert } from '../collapsibleAlert';
import { ThemeCard } from '../cards';
import { H2 } from '../sections';
import { FileCard } from '../cards';
import ScrollAnimation from "react-animate-on-scroll";

import { IconProp, library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(fas)
library.add(fab)

// Return the lessons themes cards with chapters and files in a modal
function LessonsThemes({ lessons, level, levelVersion }: { lessons: any, level: string, levelVersion: string }) {
    return (
        <Row xs={1} sm={2} md={2} lg={4} id="themeCards" key={lessons.id}>
            {/* Create a col with card for each theme */}
            {lessons && lessons.themes ? (lessons.themes.map((theme: { title: string; icon: IconProp; color: string; name: string, chapters: any }) => (
                <Col className="mb-3" key={theme.name}>
                    <ThemeCard title={theme.title} icon={theme.icon} color={theme.color} name={theme.name}>
                        {/* Create a collapsible alert for each chapter in theme.chapters */}
                        {theme.chapters ? (theme.chapters.map((chapter: { title: string, name: string, files: any }) => (
                            <CollapsibleAlert title={chapter.title} icon="book" variant="secondary" key={chapter.title}>
                                <div className="d-flex justify-content-center align-items-center text-center mt-3">
                                    {/* Create a dropdown for each file in chapter.files */}
                                    {chapter.files ? (chapter.files.map((file: { title: string, color: string, type: string, versions: any }) => (
                                        <>
                                            <FileDropdownButton title={file.title} color={file.color} type={file.type} versions={file.versions} chapter={chapter.name}
                                                level={level} levelVersion={levelVersion}
                                                key={chapter + "_" + file.title} />
                                        </>
                                    ))) : (
                                        <div>
                                            <Placeholder as="p" animation="glow" size="sm" />
                                        </div>
                                    )}
                                </div>
                            </CollapsibleAlert>
                        ))) : (
                            <div>
                                <Placeholder as="p" animation="glow" size="sm" />
                            </div>
                        )}
                    </ThemeCard>
                </Col>
            ))) : (
                // If there are no lessons, display a placeholder card
                <>
                    <Col className="mb-3">
                        <ThemeCard title="" icon="question-circle" color="" name="">
                            Test
                        </ThemeCard>
                    </Col>
                    <Col className="mb-3">
                        <ThemeCard title="" icon="question-circle" color="" name="">
                            Test
                        </ThemeCard>
                    </Col>
                    <Col className="mb-3">
                        <ThemeCard title="" icon="question-circle" color="" name="">
                            Test
                        </ThemeCard>
                    </Col>
                    <Col className="mb-3">
                        <ThemeCard title="" icon="question-circle" color="" name="">
                            Test
                        </ThemeCard>
                    </Col>
                </>
            )}
        </Row>
    );
}

// Return the lessons files table with each line for each chapter
function LessonsFileTable({ lessons, level, levelVersion }: { lessons: any, level: string, levelVersion: string }) {

    // Filter the table when the user types in the search bar
    const [search, setSearch] = useState('');
    const [filteredData, setFilteredData] = useState(lessons);
    useEffect(() => {
        if (search === '') {
            setFilteredData(lessons);
        } else {
            // Filter by theme.title, chapter.title
            const filtered = {
                connected: lessons.connected,
                themes: lessons.themes.filter((theme: { title: string, chapters: any }) => {
                    return theme.title.toLowerCase().includes(search.toLowerCase()) || theme.chapters.filter((chapter: { title: string }) => {
                        return chapter.title.toLowerCase().includes(search.toLowerCase());
                    }).length > 0;
                })
            };
            setFilteredData(filtered);
        }
    }, [search, lessons]);


    return (
        <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
            {/* Search bar */}
            <InputGroup className="mb-3" id="searchFiles">
                <InputGroup.Text id="searchIcon">
                    <FontAwesomeIcon icon="search" />
                </InputGroup.Text>
                <Form.Control
                    id="searchInput"
                    placeholder="Rechercher un document"
                    aria-label="Recherche"
                    aria-describedby="searchIcon"
                    onChange={(e) => {
                        setSearch(e.target.value);
                    }
                    }
                />
            </InputGroup>

            {/* Table with the files */}
            <Table className="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th className='text-center' style={{ width: '100px' }}>Thème</th>
                        <th>Séquence</th>
                        <th className='text-center'>Documents</th>
                    </tr>
                </thead>
                <tbody>
                    {/* Create a row for each chapter */}
                    {filteredData && filteredData.themes ? (filteredData.themes.map((theme: { title: string; icon: IconProp; color: string; name: string, chapters: any }) => (
                        theme.chapters && (theme.chapters.map((chapter: { title: string, name: string, files: any, sources: any }) => (
                            <tr key={chapter.name}
                                className='align-middle'>
                                <td className='text-center'>
                                    <FontAwesomeIcon icon={theme.icon} style={{ color: theme.color, fontSize: '1.5rem' }} />
                                </td>
                                <td>{chapter.title}</td>
                                <td className='text-center'>
                                    {chapter.files && (chapter.files.map((file: { title: string, color: string, type: string, versions: any }) => (
                                        // Add a dropdown button for each file in chapter.files
                                        // fileDropdownButton({ title: file.title, color: file.color, type: file.type, versions: file.versions, chapter: chapter.name, connected: lessons.connected })
                                        <FileDropdownButton title={file.title} color={file.color} type={file.type} versions={file.versions} chapter={chapter.name} connected={filteredData.connected}
                                            level={level} levelVersion={levelVersion}
                                            key={chapter + "_" + file.title} />
                                    )))}
                                    {/* If sources is set, add the button */}
                                    {chapter.sources && (
                                        chapter.sources.authorized ? (
                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Sources</Tooltip>}>
                                                <a href={"/view?level=" + level + "&version=" + levelVersion + "&file=sources/" + chapter.name + "-sources.zip"} target="_blank" rel="noreferrer" className="btn btn-sm m-1 btn-secondary">
                                                    <FontAwesomeIcon icon="box-open" />
                                                </a>
                                            </OverlayTrigger>
                                        ) : (
                                            <OverlayRestrictedMessage connected={filteredData.connected}>
                                                <a href={"/contact?request=access&message=" + chapter.name + "-sources.zip"}>
                                                    <a className="btn btn-sm m-1 btn-secondary disabled">
                                                        <FontAwesomeIcon icon="box-open" />
                                                    </a>

                                                </a>
                                            </OverlayRestrictedMessage>
                                        )
                                    )}
                                </td>
                            </tr>
                        )))))) : (
                        <>
                            <tr className='align-middle'>
                                <td className='text-center'>
                                    <Placeholder animation="glow">
                                        <Placeholder xs={2} /><br />
                                    </Placeholder>
                                </td>
                                <td className='text-center'>
                                    <Placeholder animation="glow">
                                        <Placeholder xs={6} /><br />
                                    </Placeholder>
                                </td>
                                <td className='text-center'>
                                    <Placeholder.Button variant="primary" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                    <Placeholder.Button variant="success" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                    <Placeholder.Button variant="info" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                </td>
                            </tr>
                            <tr className='align-middle'>
                                <td className='text-center'>
                                    <Placeholder animation="glow">
                                        <Placeholder xs={2} /><br />
                                    </Placeholder>
                                </td>
                                <td className='text-center'>
                                    <Placeholder animation="glow">
                                        <Placeholder xs={10} /><br />
                                    </Placeholder>
                                </td>
                                <td className='text-center'>
                                    <Placeholder.Button variant="primary" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                    <Placeholder.Button variant="success" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                    <Placeholder.Button variant="warning" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                </td>
                            </tr>
                        </>
                    )}
                </tbody>
            </Table>
        </ScrollAnimation>
    );
}

// Return the additional files section with cards for each section
function LessonsAdditionalFiles({ lessons }: { lessons: any }) {
    return (
        // For each section in additionalFiles, add a <H2> and add the files cards
        <>
            {lessons && lessons.additional_files ? (lessons.additional_files.map((section: { section: string, files: any }) => (
                <div key={section.section}>
                    <H2>{section.section}</H2>
                    {section.files ? (section.files.map((file: {
                        title: string, icon: IconProp,
                        description: string, buttons: any
                    }) => (
                        <FileCard id={file.title} title={file.title} icon={file.icon} description={file.description} buttons={file.buttons} key={file.title} />
                    ))) : (
                        <div>
                            <Placeholder as="p" animation="glow" size="sm" />
                        </div>
                    )}
                </div>
            ))) : (
                <>
                    <H2>Documents supplémentaires</H2>
                    <FileCard id="" title="" icon="question-circle" description="" buttons={[]} />
                </>
            )}
        </>
    );
}



// Return a dropdown button for each file type (lessons, exercises, ...). The dropdown contains the different versions of this file (teacher, student, ...)
function FileDropdownButton({ title, color, type, versions, chapter, connected, level, levelVersion }: { title: string, color: string, type: string, versions: any, chapter: string, connected?: boolean, level: string, levelVersion: string }) {
    if (versions.length === 0) {
        return
    }
    else if (versions.length === 1) {
        // Return a button if there is only one version
        return (
            <a href={"/view?level=" + level + "&version=" + levelVersion + "&file=" + chapter + "_" + type + "-" + versions[0].audience + "." + versions[0].ext} target="_blank" rel="noreferrer" className={"btn btn-sm m-1 btn-" + color}>
                <span className="fw-bold">{title}</span>
            </a>
        )
    } else {
        // Return a dropdown if there are multiple versions
        return (
            <Dropdown className='d-inline-block'>
                <Dropdown.Toggle variant={color} id="dropdown-basic" className='m-1 btn-sm'>
                    <span className="fw-bold">{title}</span>
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    {versions ? (versions.map((version: { title: string, audience: string, ext: string, authorized: boolean }) =>
                        version.authorized ? (
                            <Dropdown.Item key={chapter + "_" + title + "_" + version.title} href={"/view?level=" + level + "&version=" + levelVersion + "&file=" + chapter + "_" + type + "-" + version.audience + "." + version.ext} style={{ color: "var(--bs-body)" }}>
                                {version.title}
                            </Dropdown.Item>
                        ) : (
                            <OverlayRestrictedMessage connected={connected} key={chapter + "_" + title + "_" + version.title}>
                                <span className="d-inline-block w-100">
                                    <Dropdown.Item key={version.title} style={{ color: "var(--bs-secondary)" }} href={connected ? ("/contact?request=access&message=" + chapter + "_" + type + "-" + version.audience + "." + version.ext) : "/login?notification=info&message=Veuillez vous connecter pour accéder au document."}>
                                        <div style={{
                                            display: 'flex', justifyContent: 'space-between', alignItems: 'center',
                                            width: '100%'
                                        }}>
                                            <span>{version.title}</span>
                                            <FontAwesomeIcon icon="lock" style={{ color: "var(--bs-secondary)" }} />
                                        </div>
                                    </Dropdown.Item>
                                </span>
                            </OverlayRestrictedMessage>
                        ))
                    ) : (
                        <div>
                            <Placeholder as="p" animation="glow" size="sm" />
                        </div>
                    )}
                </Dropdown.Menu>
            </Dropdown>
        )
    }
}

// Message to display when the user is not connected and the file is restricted
function OverlayRestrictedMessage({ connected, children }: { connected: boolean | undefined, children: any }) {
    return (
        <OverlayTrigger overlay={<Popover>
            <Popover.Header as="h3">
                <FontAwesomeIcon icon="lock" style={{ color: "var(--bs-secondary)" }} /> Accès restreint
            </Popover.Header>
            <Popover.Body>
                {connected ? (
                    <span>
                        Veuillez cliquer pour demander une autorisation.
                    </span>
                ) : (
                    <span>
                        Veuillez cliquer pour vous connecter.
                    </span>
                )}
            </Popover.Body>
        </Popover>}>
            {children}
        </OverlayTrigger>
    )
}

export { LessonsThemes, LessonsFileTable, LessonsAdditionalFiles };