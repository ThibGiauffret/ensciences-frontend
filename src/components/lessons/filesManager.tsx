// Components
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import InputGroup from 'react-bootstrap/InputGroup';
import { Modal, Pagination, Table } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { useToasts } from 'react-bootstrap-toasts';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import { Dropdown } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useEffect, useState } from 'react';
import { deleteFile } from '../../functions/lessons/deleteFile';
import { listLessonsFiles } from '../../functions/lessons/listLessonsFiles';
import { uploadFile } from '../../functions/lessons/uploadFile';


// Lessons files manager
function FilesManager({ level, version }: { level: string, version: string }) {
    // Toasts
    const toasts = useToasts();

    // Show/hide the modal
    const [show, setShowEdit] = useState(false);
    const handleCloseEdit = () => setShowEdit(false);
    const handleShowEdit = () => setShowEdit(true);

    // Files and folders lists
    const [files, setFiles] = useState<FilesManagerFiles[]>([]);
    const [folders, setFolders] = useState([]);

    useEffect(() => {
        // Get the files on component mount
        listLessonsFiles(level, version).then((filesData: any) => {
            if (filesData) {
                if (filesData.status === 'success') {
                    setFiles(filesData.files);
                    setFolders(filesData.folders);
                } else if (filesData.status === 'not_authorized') {
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'Vous n\'êtes pas autorisé à accéder à la liste des fichiers.',
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 3000,
                        },
                    });
                } else {
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'Une erreur est survenue lors du chargement de la liste des fichiers : ' + filesData.status,
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 3000,
                        },
                    });
                }
            }
        });
    }, []);

    // Handle the upload of a file
    const handleUpload = (e: any) => {
        uploadFile(level, version, e.target.folder.value, e.target.file.files[0]).then((data: any) => {
            if (data.status === 'success') {
                toasts.show({
                    headerContent: 'Succès',
                    bodyContent: data.message,
                    toastProps: {
                        bg: 'success',
                        autohide: true,
                        delay: 3000,
                    },
                });
                // Refresh the files
                listLessonsFiles(level, version).then((filesData: any) => {
                    if (filesData) {
                        if (filesData.status === 'success') {
                            setFiles(filesData.files);
                            setFolders(filesData.folders);
                        } else {
                            // Display an error toast
                            toasts.show({
                                headerContent: 'Erreur',
                                bodyContent: 'Une erreur est survenue lors du chargement de la liste des fichiers : ' + filesData.status,
                                toastProps: {
                                    bg: 'danger',
                                    autohide: true,
                                    delay: 3000,
                                },
                            });
                        }
                    }
                });
            } else {
                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: 'Une erreur est survenue lors de l\'envoi du fichier : ' + data.status,
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 3000,
                    },
                });
            }
        });
    }


    return (
        <>
            {/* Dropdown Item */}
            <Dropdown.Item className='btn-sm me-2'
                onClick={() => {
                    handleShowEdit();
                }}>
                <FontAwesomeIcon icon="file" />&nbsp;Gérer les fichiers
            </Dropdown.Item>
            {/* Modal */}
            <Modal show={show} fullscreen={true} onHide={handleCloseEdit}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        <FontAwesomeIcon icon="file" className="me-2" />
                        Gérer les fichiers
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <h5 className="mb-3">Ajouter un fichier</h5>
                    <Form onSubmit={(e) => {
                        e.preventDefault();
                        handleUpload(e);
                    }}>
                        {/* Upload form */}
                        <InputGroup className="mb-3" size="sm">
                            {/* File */}
                            <Form.Control type="file" name="file" />
                            {/* Folder select */}
                            <Form.Select aria-label="Sélectionner le dossier" style={{ maxWidth: '200px' }} id="folderSelect" name="folder">
                                {folders.map((folder) => (
                                    <option key={folder}>{folder}</option>
                                ))}
                            </Form.Select>
                            {/* Upload button */}
                            <Button variant="success" type="submit">
                                <FontAwesomeIcon icon="upload" />
                            </Button>
                        </InputGroup>
                    </Form>

                    <hr />

                    <h5 className="mb-3">Gestion des fichiers</h5>
                    {/* Files manager table */}
                    <FilesManagerContent json={files} level={level} version={version} key={`${level}-${version}-${new Date().getTime()}`} />
                </Modal.Body>
            </Modal>
        </>
    );

}

// Files manager table
function FilesManagerContent({ json, level, version }: { json: FilesManagerFiles[], level: string, version: string }) {

    // Toasts
    const toasts = useToasts();

    // Files and other states
    const [files, setFiles] = useState<FilesManagerFiles[]>(json);
    const [page, setPage] = useState(1);
    const [numberPerPage, setNumberPerPage] = useState(10);
    const [numberOfPages, setNumberOfPages] = useState(0);
    const [search, setSearch] = useState('');
    const [sort, setSort] = useState('name');
    const [desc, setDesc] = useState(false);

    useEffect(() => {
        // Calculate the number of pages
        const newNumberOfPages = getNumberOfPages(files.filter((file) => file.name.toLowerCase().includes(search.toLowerCase()) || file.dir.toLowerCase().includes(search.toLowerCase())), numberPerPage);
        setNumberOfPages(newNumberOfPages);

        // If the page is higher than the number of pages, set it to the last page
        if (page > newNumberOfPages) {
            setPage(newNumberOfPages > 0 ? newNumberOfPages : 1);
        }

        // Sort the files according to the sort and desc states
        files.sort((a, b) => {
            const multi = desc === false ? 1 : -1;
            switch (sort) {
                case 'name':
                    return a.name.localeCompare(b.name) * multi;
                case 'folder':
                    return a.path.localeCompare(b.path) * multi;
                case 'size':
                    return (a.size - b.size) * multi;
                case 'date':
                    return (a.date - b.date) * multi;
                default:
                    return 0;
            }
        });
    }, [files, numberPerPage, page, search, sort, desc]);

    // Handle the sort of the table
    const handleSort = (sortName: string) => {
        if (sort === sortName) {
            setDesc(!desc);
        } else {
            setDesc(false);
        }
        setSort(sortName);
    }

    return (
        <>
            <Row>
                <Col>
                    {/* Search bar */}
                    <InputGroup className="mb-3" size="sm">
                        <InputGroup.Text><FontAwesomeIcon icon="search" /></InputGroup.Text>
                        <Form.Control type="text" placeholder="Rechercher un fichier..." value={search} onChange={(e) => {
                            setSearch(e.target.value);
                            setPage(1);
                        }} />
                        {/* Clear button when search is not empty */}
                        {search !== '' && <Button variant="secondary" onClick={() => {
                            setSearch('');
                            setPage(1);
                        }}>
                            <FontAwesomeIcon icon="times" />
                        </Button>}
                    </InputGroup>
                </Col>
                <Col>
                    {/* Select numberPerPage */}
                    <InputGroup className="mb-3" size="sm">
                        <InputGroup.Text><FontAwesomeIcon icon="list" /></InputGroup.Text>
                        <Form.Select onChange={(e) => {
                            setNumberPerPage(parseInt(e.target.value));
                            setPage(1);
                        }}>
                            <option>10</option>
                            <option>25</option>
                            <option>50</option>
                            <option>100</option>
                        </Form.Select>
                    </InputGroup>
                </Col>
            </Row>
            {/* Table */}
            <Table striped bordered hover responsive>
                <thead>
                    <tr>
                        <th>Nom

                            {/* Sort buttons */}
                            <OverlayTrigger
                                placement="top"
                                overlay={
                                    <Tooltip id="sortTooltip">
                                        Trier par nom
                                    </Tooltip>
                                }
                            >
                                <Button variant="link" size="sm" className="ms-2" onClick={() => {
                                    handleSort('name');

                                }}>
                                    {sort === "name" && desc === true ? <FontAwesomeIcon icon="sort-alpha-down" /> : <FontAwesomeIcon icon="sort-alpha-up" />}
                                </Button>
                            </OverlayTrigger>
                        </th>
                        <th>Dossier

                            {/* Sort buttons */}
                            <OverlayTrigger
                                placement="top"
                                overlay={
                                    <Tooltip id="sortTooltip">
                                        Trier par dossier
                                    </Tooltip>
                                }
                            >
                                <Button variant="link" size="sm" className="ms-2" onClick={() => {
                                    handleSort('folder');
                                }}>
                                    {sort === "folder" && desc === true ? <FontAwesomeIcon icon="sort-alpha-down" /> : <FontAwesomeIcon icon="sort-alpha-up" />}
                                </Button>
                            </OverlayTrigger>
                        </th>
                        <th>Taille

                            {/* Sort buttons */}
                            <OverlayTrigger
                                placement="top"
                                overlay={
                                    <Tooltip id="sortTooltip">
                                        Trier par taille
                                    </Tooltip>
                                }
                            >
                                <Button variant="link" size="sm" className="ms-2" onClick={() => {
                                    handleSort('size');
                                }}>
                                    {sort === "size" && desc === true ? <FontAwesomeIcon icon="sort-numeric-down" /> : <FontAwesomeIcon icon="sort-numeric-up" />}
                                </Button>
                            </OverlayTrigger>
                        </th>
                        <th>Date

                            {/* Sort buttons */}
                            <OverlayTrigger
                                placement="top"
                                overlay={
                                    <Tooltip id="sortTooltip">
                                        Trier par date
                                    </Tooltip>
                                }
                            >
                                <Button variant="link" size="sm" className="ms-2" onClick={() => {
                                    handleSort('date');
                                }}>
                                    {sort === "date" && desc === true ? <FontAwesomeIcon icon="sort-numeric-down" /> : <FontAwesomeIcon icon="sort-numeric-up" />}
                                </Button>
                            </OverlayTrigger>
                        </th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {/* Display the files according to the search and pagination */}
                    {files.filter((file) => file.name.toLowerCase().includes(search.toLowerCase()) || file.dir.toLowerCase().includes(search.toLowerCase())).slice((page - 1) * numberPerPage, page * numberPerPage).map((file) => (
                        // File row
                        <tr key={file.uuid}>
                            <td><FontAwesomeIcon icon={chooseIcon(file.ext)} className="me-2" />{file.name}
                            </td>
                            <td><Badge bg="primary" onClick={
                                () => {
                                    setSearch(file.dir);
                                }
                            }
                                style={{ cursor: 'pointer' }}>{file.dir}</Badge></td>
                            <td>
                                {/* Convert size to Mo */}
                                {(file.size / 1000000).toFixed(2)} Mo
                            </td>
                            <td>
                                {/* Convert date to string */}
                                {new Date(file.date * 1000).toLocaleString('fr-FR')}
                            </td>
                            <td>
                                {/* See button */}
                                <Button variant="primary" size="sm" className="me-2" href={file.path} target="_blank">
                                    <FontAwesomeIcon icon="eye" />
                                </Button>
                                {/* Copy link button */}
                                <Button variant="secondary" size="sm" className="me-2" onClick={() => {
                                    navigator.clipboard.writeText(file.path);
                                    // Display a success toast
                                    toasts.show({
                                        headerContent: 'Succès',
                                        bodyContent: 'Lien copié dans le presse-papiers.',
                                        toastProps: {
                                            bg: 'success',
                                            autohide: true,
                                            delay: 3000,
                                        },
                                    });
                                }}>
                                    <FontAwesomeIcon icon="link" />
                                </Button>
                                {/* Delete button */}
                                <Button variant="danger" size="sm" className="me-2" onClick={() => {
                                    deleteFile(level, version, file.dir, file.name).then((data: any) => {
                                        if (data.status === 'success') {
                                            // Remove the file from the list
                                            setFiles(files.filter((f) => f.uuid !== file.uuid));
                                            toasts.show({
                                                headerContent: 'Succès',
                                                bodyContent: "Fichier supprimé avec succès.",
                                                toastProps: {
                                                    bg: 'success',
                                                    autohide: true,
                                                    delay: 3000,
                                                },
                                            });
                                        } else {
                                            toasts.show({
                                                headerContent: 'Erreur',
                                                bodyContent: 'Une erreur est survenue lors de la suppression du fichier : ' + data.status,
                                                toastProps: {
                                                    bg: 'danger',
                                                    autohide: true,
                                                    delay: 3000,
                                                },
                                            });
                                        }
                                    });
                                }}>
                                    <FontAwesomeIcon icon="trash" />
                                </Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            <Paginations numberOfPages={numberOfPages} activePage={page} changePage={setPage} />
        </>
    );
}

// Get the number of pages
function getNumberOfPages(json: FilesManagerFiles[], numberPerPage: number) {
    return Math.ceil(json.length / numberPerPage);
}

// Pagination component
function Paginations({ numberOfPages, activePage, changePage }: { numberOfPages: number, activePage: number, changePage: (page: number) => void }) {
    return (
        <Pagination size="sm">
            <Pagination.Prev onClick={() => {
                if (activePage > 1) {
                    changePage(activePage - 1);
                }
            }} />
            {Array.from(Array(numberOfPages).keys()).map((page) => {
                return <Pagination.Item key={page} active={page === activePage - 1} onClick={() => {
                    changePage(page + 1);
                }}
                >{page + 1}</Pagination.Item>;
            })}
            <Pagination.Next onClick={() => {
                if (activePage < numberOfPages) {
                    changePage(activePage + 1);
                }
            }} />
        </Pagination>
    );
}

// Choose the icon according to the file extension
function chooseIcon(ext: string) {
    switch (ext) {
        case 'pdf':
            return 'file-pdf';
        case 'doc':
        case 'docx':
            return 'file-word';
        case 'xls':
        case 'xlsx':
            return 'file-excel';
        case 'ppt':
        case 'pptx':
            return 'file-powerpoint';
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            return 'file-image';
        case 'mp3':
        case 'wav':
            return 'file-audio';
        case 'mp4':
        case 'avi':
        case 'mov':
            return 'file-video';
        case 'zip':
        case 'rar':
        case '7z':
            return 'file-archive';
        default:
            return 'file';
    }
}
export default FilesManager;