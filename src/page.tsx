import ReactDOM from 'react-dom/client'

// Components
import Topbar from "./components/topbar";
import { hideLoader } from "./components/loader";
import { ToastsProvider as BootstrapToastsProvider } from 'react-bootstrap-toasts';
import Container from 'react-bootstrap/Container';
import Footer from "./components/footer";
import Header from "./components/header";
import Notification from './components/notification';

// Pages
import Home from "./pages/home";
import Login from "./pages/login";
import SecondePC from "./pages/seconde_pc";
import FileViewer from "./pages/view";

// Functions
import { getTheme, setTheme } from './functions/theme/setTheme';

// Router
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

// CSS
import './globals.scss';

// Create the router
const router = createBrowserRouter([
  {
    path: "/",
    element: <Home title="Accueil" />,
  },
  {
    path: "/login",
    element: <Login title="Connexion" />,

  },
  {
    path: "/seconde_pc",
    element: <SecondePC title="Seconde PC" />
  },
  {
    path: "/view",
    element: <FileViewer title="Visionneuse" />
  }
]);

// If /login is in the URL, redirect to the login page (no topbar, footer, etc.)
if (window.location.pathname === "/login" || window.location.pathname === "/view") {
  ReactDOM.createRoot(document.getElementById('root')!).render(
    <BootstrapToastsProvider toastContainerProps={{ position: 'bottom-end', className: 'p-2' }}
      limit={5}>
      <>
        <div id="wrapper" className='pb-0'>
          <RouterProvider router={router} />
        </div>
        <Notification />
      </>
    </BootstrapToastsProvider>
  )
} else {
  // Render the main content depending on the route with the topbar, footer, etc.
  ReactDOM.createRoot(document.getElementById('root')!).render(
    <BootstrapToastsProvider toastContainerProps={{ position: 'bottom-end', className: 'p-2' }}
      limit={5}>
      <>
        <div id="wrapper">
          <Topbar />
          <Container className="main" id="main">
            <Header name="Accueil" />
            <RouterProvider router={router} />
            <Footer />
          </Container>
        </div>
        <Notification />
      </>
    </BootstrapToastsProvider>
  )
}

// Set the theme
setTheme(getTheme());

// Hide the loader when the main content is rendered
function afterRender() {
  hideLoader();
}
requestIdleCallback(afterRender);


