// ----------------------------
// Footer component
// The page footer
// ----------------------------

import Container from "react-bootstrap/Container";
import styles from "./css/footer.module.css";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { IconProp, library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';

// Add the icons to the library
library.add(fas)
library.add(fab)

function Footer() {
  return (
    <footer className={styles.footer}>
      <Container className={styles.socialContainer}>
        <TooltipLink id="tooltip-twitter" title="Twitter" href="https://twitter.com/ThibGiauffret" className="m-1">
          <SquaredIcon icon={['fab', 'twitter']} size="18"></SquaredIcon>
        </TooltipLink>
        {/* Mastodon */}
        <TooltipLink id="tooltip-mastodon" title="Mastodon" href="https://mastodon.online/@ThibGiauffret" className="m-1">
          <SquaredIcon icon={['fab', 'mastodon']} size="18"></SquaredIcon>
        </TooltipLink>
        {/* Youtube */}
        <TooltipLink id="tooltip-youtube" title="Youtube" href="https://www.youtube.com/channel/UCGC82VWz8RDKSH4IpvU5M-g" className="m-1">
          <SquaredIcon icon={['fab', 'youtube']} size="18"></SquaredIcon>
        </TooltipLink>
        {/* Framagit */}
        <TooltipLink id="tooltip-framagit" title="Framagit" href="https://framagit.org/ThibGiauffret" className="m-1">
          <SquaredIcon icon={['fab', 'gitlab']} size="18"></SquaredIcon>
        </TooltipLink>
        {/* Forge apps education */}
        <TooltipLink id="tooltip-forge" title="Forge des communs numériques éducatifs" href="https://forge.apps.education.fr/thibaultgiauffret" className="m-1">
          <SquaredIcon icon={['fas', 'code-branch']}
            size="18"></SquaredIcon>
        </TooltipLink>
        <span className="ps-2 pe-2">|</span>
        {/* Coffe */}
        <TooltipLink id="tooltip-coffee" title="M'offir un café" href="https://fr.tipeee.com/thibgiauffret-ensciences" className="m-1">
          <SquaredIcon icon={['fas', 'mug-hot']} size="18"></SquaredIcon>
        </TooltipLink>
      </Container>
      <Container className={styles.footerContainer}>
        <Row>
          <Col style={{ textAlign: 'center' }} className="p-3" xs={12} md={9}>
            <span>
              <FontAwesomeIcon icon={['fab', 'creative-commons-by']} /> <FontAwesomeIcon icon={['fab', 'creative-commons-nc']} /> <FontAwesomeIcon icon={['fab', 'creative-commons-sa']} /> <span className="small m-3" style={{ fontWeight: "bold" }}> Th. G © 2018 - 2024</span>
            </span>
            <p style={{ fontSize: "0.7em" }}>
              Ces documents ont, sauf mention contraire, été réalisés par mes soins. Ils s&apos;inspirent de façon plus ou moins directe de diverses sources. J&apos;autorise quiconque le souhaite à placer des liens vers le présent site mais je n&apos;autorise pas l&apos;hébergement des fichiers. Toute utilisation commerciale est interdite.
            </p>
          </Col>
          <Col style={{ textAlign: 'center' }} className="p-3" xs={12} md={3}>
            <div className="small mb-2">Propulsé par :</div>
            <span style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', fontSize: "24px" }}>
              <TooltipLink id="tooltip-next" title="Next.js" href="https://nextjs.org/">
                <img src="/vite.svg" alt="Next.js" width={23} height={23} style={{ marginTop: "-11px", marginRight: ".25rem" }}></img>
              </TooltipLink>
              <TooltipLink id="tooltip-react" title="React" href="https://reactjs.org/">
                <FontAwesomeIcon icon={['fab', 'react']} className="m-1"></FontAwesomeIcon>
              </TooltipLink>
              <TooltipLink id="tooltip-bootstrap" title="Bootstrap" href="https://react-bootstrap.netlify.app">
                <FontAwesomeIcon icon={['fab', 'bootstrap']} className="m-1"></FontAwesomeIcon>
              </TooltipLink>
              <TooltipLink id="tooltip-fontawesome" title="Font Awesome" href="https://fontawesome.com/">
                <FontAwesomeIcon icon={['fab', 'square-font-awesome']} className="m-1"></FontAwesomeIcon>
              </TooltipLink>
            </span>
          </Col>
        </Row>
      </Container>
    </footer>
  );
}

const TooltipLink = ({ id, children, title, href, className }: { id: string, children: any, title: string, href?: string, className?: string }) => (
  <OverlayTrigger overlay={<Tooltip id={id}>{title}</Tooltip>}>
    <a href={href ? href : "#"} style={{ color: "inherit" }} target="_blank" className={className}>
      {children}
    </a>
  </OverlayTrigger>
);

function SquaredIcon({ icon, size = "15" }: { icon: IconProp, size?: string }) {

  const sizeInt = parseInt(size);
  const padding = sizeInt / 6;
  const iconSize = sizeInt - padding * 2;
  const sizeStr = sizeInt + "px";
  const iconSizeStr = iconSize + "px";

  return (
    <span className="d-inline-block">
      <span style={{
        backgroundColor: "white",
        padding: "5px",
        borderRadius: "5px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: sizeStr,
        height: sizeStr
      }}>
        <FontAwesomeIcon icon={icon} style={{ color: "black", fontSize: iconSizeStr }}></FontAwesomeIcon>
      </span>
    </span>
  );
}

export default Footer;