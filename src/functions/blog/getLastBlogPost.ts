// This function fetches the last blog post from the backend and returns it as an object
async function getLastBlogPost() {
  return new Promise(async (resolve, reject) => {
    const backendUrl = import.meta.env.VITE_BACKEND_URL + 'wordpress/'
    let postResponse = null;
    let posts = null;
    try {
      postResponse = await fetch(backendUrl + '?rest_route=/wp/v2/posts&per_page=1');
      posts = await postResponse.json();
    } catch (error) {
      console.error('Error fetching posts', error);
      reject('Error fetching posts');
    }

    if (postResponse) {
      if (posts.length > 0) {
        const authorId = posts[0].author;
        let authorResponse = null;
        let author = null;
        try {
          authorResponse = await fetch(backendUrl + `?rest_route=/wp/v2/users/${authorId}`);
          author = await authorResponse.json();
        } catch (error) {
          console.error('Error fetching author', error);
          reject('Error fetching author');
        }
        const newDate = new Date(posts[0].date);
        const newDateString = newDate.toLocaleDateString("fr-FR", {
          weekday: "long",
          year: "numeric",
          month: "long",
          day: "numeric"
        });
        resolve({
          title: posts[0].title.rendered,
          content: splitContent(posts[0].content.rendered),
          date: newDateString,
          author: author.name,
          link: posts[0].link
        });
      }
    }
    reject('No posts found');
  });
}

// A function to split the content of a blog post into a preview (if the content is longer than 30 words)
const splitContent = (content: string) => {
  // Remove html tags
  content = content.replace(/<[^>]*>/g, '');
  // If content length is less than 30 words, return the content
  if (content.split(' ').length <= 30) {
    return content;
  }
  // Split content into an array of words
  const contentArray = content.split(' ');
  const reducedContent = contentArray.slice(0, 30).join(' ');
  return reducedContent + ' [...]';
}

export { getLastBlogPost, splitContent }