// ----------------------------
// Home page
// The home page
// ----------------------------

// Imports
import { useState, useEffect } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Badge from 'react-bootstrap/Badge';
import { CollapsibleAlert } from '../components/collapsibleAlert';
import { H2 } from '../components/sections'
import { BigCircleLink, BigCircleButton } from '../components/buttons';
import { PageContainer, MenuContainer, FullContainer } from '../components/containers';
import { SimpleBlogCard } from '../components/cards';
import styles from './css/home.module.css';
import cardStyles from '../components/css/cards.module.css';
import { IconProp, library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "@fontsource/dancing-script";
import MastodonWidget from '../components/mastodonWidget';
import GitLabWidget from '../components/gitlabWidget';
import ScrollAnimation from 'react-animate-on-scroll';
import "animate.css/animate.compat.css"

// Add the icons to the library
library.add(fas)
library.add(fab)

// Calls to backend
import { getLastBlogPost } from '../functions/blog/getLastBlogPost';

// Notifications
import { useToasts } from 'react-bootstrap-toasts';

function Home(props: { title: string }) {

  // Set the title
  useEffect(() => {
    const setTitle = () => {
      document.title = props.title + " | EnSciences";
      const header = document.getElementById('pageHeader');
      if (header) {
        header.innerHTML = props.title;
      }
    };
    setTitle();
  }, []);

  // Toasts
  const toasts = useToasts();

  // Initialize the last blog post state
  const [lastBlogPost, setLastBlogPost] = useState({ title: "Titre du dernier article", content: "Contenu du dernier article", date: "", link: "", author: "" });

  // Get the last blog post
  useEffect(() => {
    getLastBlogPost().then((data: any) => {
      setLastBlogPost(data);
    }).catch((error) => {
      console.error(error);
      toasts.show({
        headerContent: 'Erreur',
        bodyContent: 'Impossible de récupérer le dernier article du blog',
        toastProps: {
          bg: 'danger',
          autohide: true,
          delay: 3000,
        },
      });
    });
  }, []);

  return (
    <PageContainer>
      <FullContainer>
        <Row>
          <Col xs={12} md={12} lg={8} className="mb-3">
            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
              <H2>Bienvenue sur EnSciences !</H2>

              <p>Ce site personnel met à disposition des documents de <strong>sciences-physiques et d&apos;informatique</strong> à destination des élèves de lycée et du supérieur. Vous trouverez aussi quelques documents personnels pour la préparation des <strong>concours de l&apos;enseignement</strong> et des <strong>ressources numériques</strong> variées (animations, Python, Arduino...).</p>

              <CollapsibleAlert icon="exclamation-circle" title="Concernant l'accès et le partage des documents..." variant="secondary">
                <p>
                  Ces ressources ont, sauf mention contraire, été réalisées par mes soins. Elles s&apos;inspirent de façon plus ou moins directe de diverses sources. J&apos;autorise quiconque le souhaite à <b>placer des liens vers le présent site mais je n&apos;autorise pas l&apos;hébergement des fichiers</b>. Toute utilisation commerciale est interdite.
                  <br />
                  Même si la plupart des documents sont en libre accès, certains sont cependant en accès restreint (lien ou bouton grisé) : une autorisation sera alors requise. Pour l&apos;obtenir, veuillez créer un compte puis me contacter <a href="/contact">ici</a>.
                  <hr />
                  Pour une navigation fluide sur le site, je recommande <FontAwesomeIcon icon={["fab", "chrome"]} /> Google Chrome ou <FontAwesomeIcon icon={["fab", "firefox"]} /> Mozilla Firefox.
                </p>
              </CollapsibleAlert>

              <p>
                Il y a des points à reprendre (coquilles ou autres erreurs...) donc <b>n&apos;hésitez pas à me contacter pour me les signaler</b>. Je serais ravi d&apos;avoir vos retours&nbsp;!
              </p>

              <p>
                J&apos;espère cependant que vous trouverez des choses utiles !
              </p>
              <p className={styles.signature}>Th. G</p>
            </ScrollAnimation>

          </Col>
          <Col className="mb-3">
            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
              <H2>Restons en contact</H2>

              <Row xs={2} md={3} lg={2} xl={3}>
                <Col>

                  <BigCircleLink color="#007bff"
                    icon="twitter" href="https://twitter.com/ThibGiauffret" target="_blank" className="mx-auto my-3" tooltip='Twitter' />
                </Col>
                <Col>
                  <BigCircleLink color="#8c8dff"
                    icon="mastodon" href="https://mastodon.online/@ThibGiauffret" target="_blank" className="mx-auto my-3" tooltip='Mastodon' />
                </Col>
                <Col>
                  <BigCircleLink color="#ff0000"
                    icon="youtube" href="https://www.youtube.com/channel/UCGC82VWz8RDKSH4IpvU5M-g" target="_blank" className="mx-auto my-3" tooltip='Youtube' />

                </Col>
                <Col>
                  <BigCircleLink color="#292961"
                    icon="gitlab" href="https://framagit.org/ThibGiauffret" target="_blank" className="mx-auto my-3" tooltip='Framagit' />
                </Col>
                <Col>
                  <BigCircleLink color="#F17361" icon="code-branch" href="https://forge.apps.education.fr/thibaultgiauffret" target="_blank" className="mx-auto my-3" tooltip='Forge des communs numériques éducatifs' />
                </Col>
                <Col>
                  <div className="d-flex flex-row align-items-center">
                    <BigCircleLink color="#5da500"
                      icon="envelope" href="/contact" target="_blank" className="mx-auto my-3" tooltip='Envoyer un message' />
                  </div>
                </Col>
              </Row>
            </ScrollAnimation>
          </Col>
        </Row>
      </FullContainer>
      <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
        <MenuContainer>
          <Row xs={1} md={2} lg={3}>
            {/* Cours */}
            <MenuItem id="cours" icon="chalkboard-teacher" links={{
              "2nde PC": "/seconde_pc",
              "2nde SNT": "/seconde_snt",
              "Tle Spé": "/terminale_spe",
              "Tle ES": "/terminale_es",
              "Tle STI2D": "/terminale_sti2d",
            }} title="Cours">
              <span className="small" style={{ textAlign: "left" }}>
                Quelques documents de cours pour le lycée général/technologique et le supérieur.
              </span>
            </MenuItem>
            {/* Animations */}
            <MenuItem id="animations" icon="cube" links={{}} title="Animations" link="/animation">
              <span className="small" style={{ textAlign: "left" }}>
                Des animations et simulations en 2D et 3D pour découvrir ou illustrer des notions.
              </span>
            </MenuItem>
            {/* Objets connectés */}
            <MenuItem id="objets" icon="microchip" links={{
              "Laboratoire connecté": "https://labo.ensciences.fr/",
              "Station météo": "https://meteo.ensciences.fr/"
            }} title="Objets connectés">
              <span className="small" style={{ textAlign: "left" }}>
                Divers projets à base d&apos;objets connectés à base de Raspberry, Arduino...
              </span>
            </MenuItem>
            {/* Ressources */}
            <MenuItem id="ressources" icon="box-open" links={{
              "Banque de ressources": "https://banques.ensciences.fr",
              "CAPES (2018)": "/capes",
              "Agrégation de Chimie (2019)": "/agregation",
              "Autres ressources": "/ressources"
            }} title="Ressources">
              <span className="small" style={{ textAlign: "left" }}>
                Banque de ressources, documents pour CAPES/Agrégation et ressources diverses (schémas Inkscape, styles LaTeX...).
              </span>
            </MenuItem>
            {/* Applications en ligne */}
            <MenuItem id="applications" icon="rocket" links={{
              "Bacasable": "https://www.ensciences.fr/addons/bacasable",
              "BlocAlgo": "/blocalgo",
              "Tableau": "https://www.tableau.ensciences.fr/",
              "PhysNet": "/physnet",
              "ChronoPhys": "/chronophys"
            }} title="Applications en ligne et logiciels">
              <span className="small" style={{ textAlign: "left" }}>
                Quelques logiciels et outils en ligne comme supports pour l&apos;enseignement en Sciences.
              </span>
            </MenuItem>
            {/* Blog */}
            <MenuItem id="blog" icon="feather-pointed" links={{}}
              title="Blog" link="/blog">
              <span className="small" style={{ textAlign: "left" }}>
                Un espace d&apos;échange d&apos;idées, de concepts, de projets autour de la Physique-Chimie et des outils numériques&nbsp;...
              </span>
            </MenuItem>
          </Row>
        </MenuContainer>
      </ScrollAnimation>

      <FullContainer>
        <Row xs={1} sm={1} md={2}>
          <Col className="mb-3">
            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
              <H2>Dernier billet de blog</H2>
              <SimpleBlogCard title={lastBlogPost.title} author={lastBlogPost.author} date={lastBlogPost.date} href={lastBlogPost.link}>
                {/* Safe ? */}
                <div dangerouslySetInnerHTML={{ __html: lastBlogPost.content }} />
              </SimpleBlogCard>
            </ScrollAnimation>

            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
              <H2>Dernière vidéo</H2>
              <iframe className={cardStyles.card} width="100%" height="315" src="https://www.youtube-nocookie.com/embed/SlQXyTPQKOU" title="Dernière vidéo" allowFullScreen></iframe>
            </ScrollAnimation>

            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
              <H2>Nouveautés du site</H2>
              <ul className={styles.newsCheckList}>
                <NewsItem date="19/06/2024" title="Nouveauté 1" />
                <NewsItem date="19/06/2024" title="Nouveauté 2" />
                <NewsItem date="19/06/2024" title="Nouveauté 3" />
              </ul>
            </ScrollAnimation>
          </Col>
          <Col className="mb-3">
            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
              <H2>Publications</H2>
            </ScrollAnimation>

            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
              <MastodonWidget></MastodonWidget>
            </ScrollAnimation>

            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
              <GitLabWidget title="Framagit"
                server="https://framagit.org/" username="ThibGiauffret"></GitLabWidget>
            </ScrollAnimation>

            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
              <GitLabWidget title="Forge des communs numériques éducatifs" server="https://forge.apps.education.fr/" username="thibaultgiauffret"></GitLabWidget>
            </ScrollAnimation>
          </Col>
        </Row>
      </FullContainer>
    </PageContainer >
  );
}

function MenuItem(props: { title: string, id: string, icon: IconProp, children: any, links: any, link?: string }) {
  const id = props.id;
  return (
    <Col style={{
      display: "flex",
      flexDirection: "row",
      justifyContent: "left",
      alignItems: "center",
      marginBottom: "8px",
      marginTop: "8px"
    }}>
      <div>
        <BigCircleButton color="#ba85fc" textColor="#343434" icon={props.icon as string} onClick={() => {
          if (props.link) {
            // router.push(props.link);
          } else {
            const links = document.getElementById(id + "-links");
            const text = document.getElementById(id + "-text");
            if (links && text) {
              links.classList.toggle("d-none");
              text.classList.toggle("d-none");
            }
          }
        }} />
      </div>
      <div id={id + "-links"} className="d-inline d-none mx-3">
        <b>{props.title}</b><br />
        {Object.keys(props.links).map((key) => {
          return (
            <div key={key} className={styles.menuBadgeDiv}>
              <Badge pill bg="primary" className={styles.menuBadge}>
                <a href={props.links[key]} className={styles.menuLink}>{key}</a>
              </Badge>
            </div>
          )
        }
        )}
      </div>
      <div id={id + "-text"} className="d-inline-block mx-3" style={{
        textAlign: "left"
      }}>
        <b>{props.title}</b><br />
        {props.children}
      </div>
    </Col>
  )
}

function NewsItem(props: { date: string, title: string }) {
  return (
    <li><FontAwesomeIcon icon="check-circle" />&nbsp;&nbsp;<b>{props.date}</b> : {props.title}</li>
  )
}

// Export
export default Home;
