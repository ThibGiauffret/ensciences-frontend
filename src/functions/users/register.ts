// Register function sends a POST request to the backend to register the user.

async function register(name: string, surname: string, username: string, email: string, password: string, passwordConfirmation: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_BACKEND_URL
    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'api/register.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ name, surname, username, email, password, passwordConfirmation }),
        });
        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            return { 'message': "Inscription réussie. Veuillez vous connecter.", "status": "success" };
        } else {
            return { 'errors': data.errors, "status": "error" };
        }
    } catch (error) {
        console.error("Erreur lors de l'inscription:", error);
        return { 'message': "Erreur lors de l'inscription.", "status": "error" };
    }
}

export default register;