// removeParam function removes the specified parameter from the URL and updates the URL in the browser.

function removeParam(sourceURL: string, key: Array<string> | string) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (key instanceof Array && key.includes(param) || key === param) {
                params_arr.splice(i, 1);
            }
        }
        if (params_arr.length) rtn = rtn + "?" + params_arr.join("&");
    }
    
    // Update the URL if the notification is not null
    window.history.replaceState({}, document.title, rtn);
}

export default removeParam;