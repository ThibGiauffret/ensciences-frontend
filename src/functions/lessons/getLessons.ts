async function getLessons(lessonsId: string, raw: boolean = false) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_BACKEND_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'api/loadLessons.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ lessonsId, raw }),
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            if (!data.writableCache) {
                console.error('The cache is not writable... Contact the administrator.');
            }
            return { "status": "success", "lessons": data.lessons, "connected": data.connected };
        } else {
            return { 'status': data.status, 'connected': data.connected, 'usingCache': data.usingCache, "json": data.json };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { getLessons };