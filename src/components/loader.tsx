// A loader screen that is displayed while the main content is loading
import styles from './css/loader.module.css';
import { createRoot } from 'react-dom/client';


const loader = createRoot(document.getElementById('loader')!);
loader.render(
    <Loader />
);

function Loader() {
    return (
        <div className={styles.loader}>
            <img src="./loader.gif" alt="Loading..." className={styles.loaderImage} />

            {/* <div className={styles.loaderText}>
                <span>Chargement en cours...</span>
            </div> */}

            <div className={styles.loaderCredits}>
                <img src="./favicon_color.svg" alt="Logo" className={styles.loaderLogo} /><br />
                <span className={styles.loaderBrand}>EnSciences<br />
                    <span className={styles.loaderDate}>Th. G &copy; 2018&nbsp;-&nbsp;2024</span>
                </span>
            </div>
        </div>
    );
};

function hideLoader() {
    document.getElementById('loader')!.style.display = 'none';
}

export { hideLoader };