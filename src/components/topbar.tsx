// ----------------------------
// Topbar component
// The topbar of the website, containing the main navigation links
// ----------------------------

// Imports
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Dropdown from 'react-bootstrap/Dropdown';
import { Alert } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import styles from './css/topbar.module.css';
import { IconProp, library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(fas)
library.add(fab)

// Functions
import getUserInfos from '../functions/users/getUserInfos';
import { switchTheme, getTheme } from '../functions/theme/setTheme';

// Variables
const backendUrl = import.meta.env.VITE_BACKEND_URL;

// ----------------------------
// Topbar component
// ----------------------------
function Topbar() {

  // State variables for user info and admin status
  const [userInfo, setUserInfo] = useState<{ username: string, role: string } | null>(null);
  const [userIsAdmin, setUserIsAdmin] = useState(false);
  const [theme, setTheme] = useState('light');

  // Get the user infos when the component is mounted
  useEffect(() => {
    // Get the theme from the local storage
    setTheme(getTheme());

    getUserInfos().then((response: any) => {
      if (response.data) {
        setUserInfo(response.data);
        if (response.data.role === 'administrator') {
          setUserIsAdmin(true);
        }
      }
    });
  }, []);


  return (
    // Navbar
    <Navbar expand="lg" className={styles.Navbar} sticky="top" variant="dark">

      {/* Main container */}
      <Container className={styles.NavbarContainer}>

        {/* Brand with logo */}
        <Navbar.Brand href="/" className="d-flex align-items-center">
          <img src="./favicon_color.svg" alt="EnSciences" width="30" height="30" className={styles.BrandIcon + " d-inline-block align-top"} />
          <span className={styles.Brand}>
            EnSciences
          </span>
        </Navbar.Brand>

        {/* Hamburger button */}
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">

            {/* Course dropdown */}
            <CustomNavDropdown icon="chalkboard-teacher" title="Cours" id="cours">
              <NavDropdown.Item href="/seconde_pc?version=2024" className={styles.NavDropdownItem}>
                <span className={styles.NavDropdownItemText}>2nde</span>
                <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
              </NavDropdown.Item>
              <NavDropdown.Item href="/terminale_spe?version=2024" className={styles.NavDropdownItem}>
                <span className={styles.NavDropdownItemText}>Tle Spé</span>
                <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
              </NavDropdown.Item>

              <NavDropdown.Divider />

              <CustomNavSecondaryDropdown icon="archive" title="Archives">
                <Dropdown.Item disabled>
                  <span className={styles.NavDropdownItemLabel}>2023-2024</span>
                </Dropdown.Item>
                <NavDropdown.Item href="/terminale_sti2d?version=2023" className={styles.NavDropdownItem}>
                  <span className={styles.NavDropdownItemText}>Tle STI2D</span>
                  <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                </NavDropdown.Item>
                <NavDropdown.Item href="/terminale_es" className={styles.NavDropdownItem}>
                  <span className={styles.NavDropdownItemText}>Tle ES</span>
                  <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                </NavDropdown.Item>
                <Dropdown.Item href="/seconde_snt" className={styles.NavDropdownItem}>
                  <span className={styles.NavDropdownItemText}>2nde</span>
                  <span className={styles.NavDropdownItemTextMuted}>Sciences Numériques et Technologie</span>
                </Dropdown.Item>
                <Dropdown.Item disabled>
                  <span className={styles.NavDropdownItemLabel}>2022-2023</span>
                </Dropdown.Item>
                <Dropdown.Item href="/premiere_es?version=2022" className={styles.NavDropdownItem}>
                  <span className={styles.NavDropdownItemText}>1ère ES</span>
                  <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                </Dropdown.Item>
                <Dropdown.Item disabled>
                  <span className={styles.NavDropdownItemLabel}>2021-2022</span>
                </Dropdown.Item>
                <Dropdown.Item href="/bts1_ms" className={styles.NavDropdownItem}>
                  <span className={styles.NavDropdownItemText}>BTS MS</span>
                  <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                </Dropdown.Item>
              </CustomNavSecondaryDropdown>

              <NavDropdown.Divider />

              <NavDropdown.Item href="/cahier_texte" className={styles.NavDropdownItem}>
                <FontAwesomeIcon icon="calendar-check" /> Cahier de texte
              </NavDropdown.Item>

              <NavDropdown.Item href="https://physnet.ensciences.fr" className={styles.NavDropdownItem}>
                <FontAwesomeIcon icon="school" /> PhysNet
              </NavDropdown.Item>

              <NavDropdown.Item href="https://www.moodle.ensciences.fr/" className={styles.NavDropdownItem}>
                <FontAwesomeIcon icon="graduation-cap" /> Moodle
              </NavDropdown.Item>
            </CustomNavDropdown>

            {/* Animations */}
            <Nav.Link href="/animations" className={styles.mainNavLink}>
              <FontAwesomeIcon icon="cube" /> Animations
            </Nav.Link>

            {/* Applications */}
            <Nav.Link href="/applications" className={styles.mainNavLink}>
              <FontAwesomeIcon icon="rocket" /> Applications
            </Nav.Link>

            {/* Resources */}
            <CustomNavDropdown icon="box-open"
              id="ressources"
              title="Ressources">
              <NavDropdown.Item href="https://banques.ensciences.fr" className={styles.NavDropdownItem}>
                <FontAwesomeIcon icon="database" /> Banque de ressources
              </NavDropdown.Item>

              <NavDropdown.Divider />

              <CustomNavSecondaryDropdown icon="display"
                title="Logiciels">
                <Dropdown.Item href="/multicompile" className={styles.NavDropdownItem}>
                  MultiCompile
                </Dropdown.Item>
                <Dropdown.Item href="/physnet" className={styles.NavDropdownItem}>
                  PhysNet
                </Dropdown.Item>
                <Dropdown.Item href="/chronophys" className={styles.NavDropdownItem}>
                  ChronoPhys
                </Dropdown.Item>
              </CustomNavSecondaryDropdown>

              <NavDropdown.Item href="https://objets.ensciences.fr" className={styles.NavDropdownItem}>
                <FontAwesomeIcon icon="microchip" /> Objets connectés
              </NavDropdown.Item>

              <CustomNavSecondaryDropdown icon="graduation-cap" title="Concours pour l'enseignement">
                <Dropdown.Item href="/capes" className={styles.NavDropdownItem}>
                  CAPES (2018)
                </Dropdown.Item>
                <Dropdown.Item href="/agregation" className={styles.NavDropdownItem}>
                  Agrégation de Chimie (2019)
                </Dropdown.Item>
              </CustomNavSecondaryDropdown>

              <NavDropdown.Divider />

              <NavDropdown.Item href="/ressources" className={styles.NavDropdownItem}>
                Autres ressources
              </NavDropdown.Item>
            </CustomNavDropdown>

            {/* Blog */}
            <Nav.Link href="/blog" className={styles.mainNavLink}>
              <FontAwesomeIcon icon="feather-pointed" /> Blog
            </Nav.Link>

            {/* Additional links dropdown */}
            <CustomNavDropdown icon="external-link-alt" title="Liens" id="liens">
              <NavDropdown.Item href="https://www.atrium-sud.fr/web/lgt-guillaume-apollinaire-063001" className={styles.NavDropdownItem}>
                Site du Lycée Guillaume Apollinaire
              </NavDropdown.Item>
              <NavDropdown.Item href="https://www.atrium-sud.fr/" className={styles.NavDropdownItem}>
                Atrium (ENT)
              </NavDropdown.Item>
            </CustomNavDropdown>
          </Nav>

          {/* Links on the right */}
          <Nav className="ms-auto">

            {/* Change theme */}
            <Nav.Link onClick={() => {
              switchTheme();
              setTheme(getTheme());
            }} className={styles.mainNavLink} id="themeButton">
              {theme === 'light' ? <FontAwesomeIcon icon="moon" /> : <FontAwesomeIcon icon="sun" />}
              <span className={"d-inline d-lg-none"}> Thème</span>
            </Nav.Link>

            {/* Contact */}
            <Nav.Link href="/contact" className={styles.mainNavLink}>
              <FontAwesomeIcon icon="envelope" />
              <span className={"d-inline d-lg-none"}> Contact</span>
            </Nav.Link>

            {/* Android App */}
            <Nav.Link href="#" className={styles.mainNavLink}>
              <FontAwesomeIcon icon={["fab", "android"]} />
              <span className={"d-inline d-lg-none"}> Android</span>
            </Nav.Link>

            {/* Login */}
            {/* Afficher des informations de l'utilisateur ici, par exemple: */}
            {userInfo && (
              // Dropdown with user info
              <CustomNavDropdown icon="user" title={userInfo.username} id="user" align="end">
                <div className="px-3">
                  <div style={{ fontSize: "1.2rem", fontWeight: "bold", marginBottom: "5px", marginTop: "5px" }}>Bienvenue {userInfo.username} !</div>
                  <Alert variant="info" className={styles.NavDropdownItem}>
                    <FontAwesomeIcon icon="lightbulb" /> Vous êtes connecté en tant que <strong>{userInfo.role}</strong>.
                  </Alert>
                </div>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/reset_password" className={styles.NavDropdownItem}>
                  <FontAwesomeIcon icon="lock" /> Changer de mot de passe
                </NavDropdown.Item>
                {userIsAdmin && (
                  <div>
                    <NavDropdown.Item href="/admin" className={styles.NavDropdownItem}>
                      <FontAwesomeIcon icon="tachometer-alt" /> Tableau de bord
                    </NavDropdown.Item>
                    <NavDropdown.Item href={backendUrl + 'wp-admin'} className={styles.NavDropdownItem}>
                      <FontAwesomeIcon icon="tools" /> Administration de Wordpress
                    </NavDropdown.Item>
                    <NavDropdown.Item href={
                      backendUrl + 'matomo/'
                    } className={styles.NavDropdownItem}>
                      <FontAwesomeIcon icon="chart-line" /> Audiences
                    </NavDropdown.Item>
                    {/* Webmail */}
                    <NavDropdown.Item href="https://webmail.ensciences.fr" className={styles.NavDropdownItem}>
                      <FontAwesomeIcon icon="envelope" /> Webmail
                    </NavDropdown.Item>
                    <NavDropdown.Item href={backendUrl + 'files/'} className={styles.NavDropdownItem}>
                      <FontAwesomeIcon icon="file" /> Fichiers
                    </NavDropdown.Item>
                  </div>
                )}
                <NavDropdown.Divider />
                <NavDropdown.Item href="/login?logout=true" className={styles.NavDropdownItem} style={{ color: "var(--bs-danger)" }}>
                  <FontAwesomeIcon icon="sign-out-alt" /> Déconnexion
                </NavDropdown.Item>
              </CustomNavDropdown>
            )}
            {!userInfo && (
              <Nav.Link href="/login" className={styles.mainNavLink}>
                <FontAwesomeIcon icon="sign-in-alt" />
                <span className={"d-inline d-lg-none"}> Connexion</span>
              </Nav.Link>
            )}

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar >
  );
}

// ----------------------------
// Custom NavDropdown components
// @param icon: IconProp - the icon to display
// @param title: string - the title of the dropdown
// @param id: string - the id of the dropdown
// @param children: React.ReactNode - the children of the dropdown
// ----------------------------
type CustomNavDropdownProps = {
  icon: IconProp;
  title: string;
  id: string;
  children: React.ReactNode;
  align?: 'start' | 'end';
};

function CustomNavDropdown({ icon, title, id, children, align = 'start'
}: CustomNavDropdownProps) {

  // Handle hover and click events. Initialize the dropdown as closed.
  const [isOpen, setIsOpen] = useState(false);
  let timeoutId: NodeJS.Timeout;

  const handleMouseEnter = () => {
    // Do nothing if the window is small
    if (window.innerWidth <= 992) return;

    // Clear the timeout and open the dropdown
    clearTimeout(timeoutId);
    setIsOpen(true);
  };

  const handleMouseLeave = () => {
    // Do nothing if the window is small
    if (window.innerWidth <= 992) return;

    // Close the dropdown after a delay
    timeoutId = setTimeout(() => setIsOpen(false), 250);
  };


  const handleDropdownClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    // Do nothing if the window is large
    if (window.innerWidth > 992) return;

    // Prevent the parent NavDropdown from closing when clicking on a SecondaryNavDropdown
    if (hasClass(event.target as Element, styles.SecondaryNavDropdown)) {
      return;
    }

    // Toggle the dropdown
    setIsOpen(!isOpen);
  }

  return (
    <NavDropdown align={align}
      title={<span><FontAwesomeIcon icon={icon} /> {title}</span>}
      id={id}
      // On width > 992px (bootstrap lg), the dropdown is shown on hover. On width <= 992px, the dropdown is shown on click.
      show={isOpen}
      onMouseEnter={() => handleMouseEnter()}
      onMouseLeave={() => handleMouseLeave()}
      onClick={(event) => handleDropdownClick(event as React.MouseEvent<HTMLDivElement, MouseEvent>)}
      className={styles.mainNavDropdown}
    >
      {children}
    </NavDropdown>
  );
}

// ----------------------------
// Custom NavSecondaryDropdown components
// @param icon: IconProp - the icon to display
// @param title: string - the title of the dropdown
// @param children: React.ReactNode - the children of the dropdown
// ----------------------------
type CustomNavSecondaryDropdownProps = {
  icon: IconProp;
  title: string;
  children: React.ReactNode;
};

function CustomNavSecondaryDropdown({ icon, title, children }: CustomNavSecondaryDropdownProps) {

  // Handle hover and click events. Initialize the dropdown as closed.
  const [isOpen, setIsOpen] = useState(false);
  let timeoutId: NodeJS.Timeout;

  const handleMouseEnter = () => {
    // Do nothing if the window is small
    if (window.innerWidth <= 992) return;

    // Clear the timeout and open the dropdown
    clearTimeout(timeoutId);
    setIsOpen(true);
  };

  const handleMouseLeave = () => {
    // Do nothing if the window is small
    if (window.innerWidth <= 992) return;

    // Close the dropdown after a delay
    timeoutId = setTimeout(() => setIsOpen(false), 250);
  };

  const handleDropdownClick = () => {
    // Do nothing if the window is large
    if (window.innerWidth > 992) return;

    // Toggle the dropdown
    setIsOpen(!isOpen);
  }

  return (
    <Dropdown
      drop="end"
      // On width > 992px (bootstrap lg), the dropdown is shown on hover. On width <= 992px, the dropdown is shown on click.
      show={isOpen}
      onMouseEnter={() => handleMouseEnter()}
      onMouseLeave={() => handleMouseLeave()}
      onClick={() => handleDropdownClick()}
    >
      <Dropdown.Toggle bsPrefix="custom" className={styles.SecondaryNavDropdown}>
        <div className={styles.SecondaryNavDropdownContent}>
          <span className={styles.SecondaryNavDropdownText}><FontAwesomeIcon icon={icon} /> {title}</span>
          <FontAwesomeIcon icon='caret-right' />
        </div>
      </Dropdown.Toggle>

      <Dropdown.Menu className={styles.SecondaryNavDropdownMenu}>
        {children}
      </Dropdown.Menu>
    </Dropdown>
  );
}

// ----------------------------
// hasClass function
// Check if an element has a specific class in a recursive way
// @param element: Element | null - the element to check
// @param className: string - the class to check
// @returns boolean - true if the class is found, false otherwise
// ----------------------------
function hasClass(element: Element | null, className: string): boolean {
  if (!element || element === document.body) {
    return false;
  } else if (element.classList.contains(className)) {
    return true;
  } else {
    return hasClass(element.parentElement, className);
  }
}

// Export the component
export default Topbar;