// Components
import { Modal } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { getLessons } from '../../functions/lessons/getLessons';
import { useToasts } from 'react-bootstrap-toasts';
import { Dropdown } from 'react-bootstrap';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useEffect, useState } from 'react';
import { deleteCache } from '../../functions/lessons/deleteCache';
import { updateLessons } from '../../functions/lessons/updateLessons'
import AdditionalFilesEditorContent from './additionalFilesEditor';
import ThemesEditorContent from './themesEditor';
import LevelEditorContent from './levelEditor';

// Theme editor (button and modal)
function LessonsEditor({ level, version, editor }: { level: string, version: string, editor: string }) {

    // Toasts
    const toasts = useToasts();

    // Show/hide modal
    const [show, setShowModal] = useState(false);
    const handleCloseEdit = (noAlert: boolean = false) => {
        if (!noAlert && modified) {
            if (confirm("Voulez-vous enregistrer les modifications ?")) {
                saveJson(true);
                setShowModal(false);
            } else {
                setShowModal(false);
            }
        } else {
            setShowModal(false);
        }
    }
    const handleShowModal = () => setShowModal(true);

    // Initial state for the lessons data
    const initialVersionState: Lessons = {
        id: level,
        title: '',
        description: '',
        versions: [{
            additional_files: [],
            themes: [],
            version: '',
            oldVersion: '',
            id: 0,
            availableAdditionalFiles: []
        }]
    };
    const [rawLessons, setRawLessons] = useState<Lessons>(initialVersionState);
    const [modified,  setModified] = useState<boolean>(false);

    // Get the raw lessons data
    useEffect(() => {
        getLessons(level, true).then((lessonsData) => {
            if (lessonsData) {
                if (lessonsData.status === 'success') {
                    // Check if the lessons data is not null (no lessons)
                    if (lessonsData.lessons !== null) {
                        setRawLessons(lessonsData.lessons)
                    }
                } else if (lessonsData.status === 'not_authorized') {
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'Vous n\'êtes pas autorisé à accéder aux données brutes des leçons.',
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 3000,
                        },
                    });
                } else {
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'Une erreur est survenue lors du chargement des données brutes des leçons.',
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 3000,
                        },
                    });
                }
            }
        });
    }, []);

    // Save the JSON data
    const saveJson = (noAlert: boolean) => {
        updateLessons(level, version, rawLessons).then((updateResult: any) => {
            toasts.show({
                headerContent: updateResult.header,
                bodyContent: updateResult.body,
                toastProps: {
                    bg: updateResult.status,
                    autohide: true,
                    delay: 3000,
                },
            });
            if (updateResult.status === 'success') {
                deleteCache(level).then((data) => {
                    if (data.status === 'success') {
                        handleCloseEdit(noAlert);
                        const currentUrl = new URL(window.location.href);
                        currentUrl.searchParams.set('notification', 'success');
                        currentUrl.searchParams.set('message', 'Les leçons ont été enregistrées avec succès.');
                        window.history.pushState({}, '', currentUrl);
                        window.location.reload();
                    } else {
                        toasts.show({
                            headerContent: 'Erreur',
                            bodyContent: 'Une erreur est survenue lors de la suppression du cache.',
                            toastProps: {
                                bg: 'danger',
                                autohide: true,
                                delay: 3000,
                            },
                        });
                    }
                });
            }
        })
    };

    return (
        <>
            {/* Dropdown Item */}
            <Dropdown.Item className='btn-sm me-2'
                onClick={() => {
                    handleShowModal();
                }}>
                {setTitle(editor)}

            </Dropdown.Item>
            {/* Modal */}
            <Modal show={show} fullscreen={true} onHide={handleCloseEdit}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {setTitle(editor)}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    {editor === 'themes' && (
                        <ThemesEditorContent version={version} json={rawLessons} setJson={setRawLessons} setModified={setModified} />
                    )}
                    {editor === 'additional_files' && (
                        <AdditionalFilesEditorContent version={version} json={rawLessons} setJson={setRawLessons} setModified={setModified} />
                    )}
                    {editor === 'versions' && (
                        <LevelEditorContent json={rawLessons} setJson={setRawLessons} setModified={setModified} />
                    )}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" className='btn-sm me-2' onClick={() => {
                        deleteCache(level).then((data) => {
                            if (data.status === 'success') {
                                toasts.show({
                                    headerContent: 'Succès',
                                    bodyContent: 'Le cache a été vidé avec succès.',
                                    toastProps: {
                                        bg: 'success',
                                        autohide: true,
                                        delay: 3000,
                                    },
                                });
                            } else {
                                toasts.show({
                                    headerContent: 'Erreur',
                                    bodyContent: 'Une erreur est survenue lors de la suppression du cache : ' + data.message,
                                    toastProps: {
                                        bg: 'danger',
                                        autohide: true,
                                        delay: 3000,
                                    },
                                });
                            }
                        });

                    }
                    }>
                        <FontAwesomeIcon icon="trash" />&nbsp;Vider le cache
                    </Button>
                    <Button variant="primary" className="btn-sm" onClick={() => saveJson(true)}>
                        <FontAwesomeIcon icon="save" />&nbsp;Enregistrer
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

function setTitle(editor: string) {
    return (
        <>
            {editor === 'themes' && (
                <>
                    <FontAwesomeIcon icon="edit" className="me-2" />Éditer les thèmes et chapitres
                </>
            )}

            {editor === 'additional_files' && (
                <>
                    <FontAwesomeIcon icon="puzzle-piece" className="me-2" />Éditer les fichiers supplémentaires
                </>
            )}

            {editor === 'versions' && (
                <>
                    <FontAwesomeIcon icon="history" className="me-2" />Éditer le niveau et les versions
                </>
            )}
        </>
    );
}

export default LessonsEditor;