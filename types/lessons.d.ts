interface Lessons {
    id: string;
    title: string;
    description: string;
    versions: Version[]
}

interface Version {
    additional_files: AdditionalFile[];
    themes: Theme[];
    version: string,
    isNew?: boolean;
    isDeleted?: boolean;
    isModified?: boolean;
    oldVersion: string;
    id: number;
    availableAdditionalFiles: AvailableAdditionalFiles[];
}

interface Theme {
    id: number;
    name: string;
    title: string;
    icon: string;
    color: string;
    chapters: Chapter[];
}

interface Chapter {
    id: number;
    name: string;
    title: string;
    files: File[];
}

interface File {
    title: string;
    color: string;
    type: string;
    versions: FileVersion[];
}

interface FileVersion {
    access_level: string;
    audience: string;
    authorized: boolean;
    date: number;
    ext: string;
    name: string;
    path: string;
    size: number;
    title: string;
}

interface AdditionalFile {
    id: number;
    section: string;
    files: AdditionalFileFiles[];
}

interface Button {
    id: number;
    title: string;
    name: string;
    icon: string;
    color: string;
    path: string;
    date: number;
    size: number
}

interface AdditionalFileFiles {
    id: number;
    icon: string;
    title: string;
    description: string;
    buttons: Button[];
}

interface AvailableAdditionalFiles {
    name: string;
    path: string;
    size: number;
    date: number;
}