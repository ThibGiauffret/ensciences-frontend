// logout function sends a POST request (with credentials) to the backend to log in the user.

async function logout() {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_BACKEND_URL;
    try {
        // Send the POST request
        await fetch(backendUrl + 'api/logout.php', {
            method: 'GET',
            credentials: 'include'
        });
        return { "status": "success" };


    } catch (error) {
        console.error('Erreur lors de la déconnexion:', error);
        return { 'status': 'error' };
    }
}

export default logout;