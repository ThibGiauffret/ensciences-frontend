// Components
import { Alert, Form, Button } from 'react-bootstrap';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "animate.css/animate.compat.css"
import { useToasts } from 'react-bootstrap-toasts';
import { deleteCache } from '../../functions/lessons/deleteCache';
library.add(fas)
library.add(fab)

function LevelInit({ level }: { level: string }) {

    // Toast
    const toasts = useToasts();

    const handleLevelInit = () => {
        // Get the values from the form
        const levelId = (document.getElementById('levelId') as HTMLInputElement).value;
        const levelTitle = (document.getElementById('levelTitle') as HTMLInputElement).value;
        const levelDescription = (document.getElementById('levelDescription') as HTMLInputElement).value;
        const versionName = (document.getElementById('versionName') as HTMLInputElement).value;

        // Add is-invalid class to the empty fields
        if (levelId === '') {
            (document.getElementById('levelId') as HTMLInputElement).classList.add('is-invalid');
        }
        if (levelTitle === '') {
            (document.getElementById('levelTitle') as HTMLInputElement).classList.add('is-invalid');
        }
        if (versionName === '') {
            (document.getElementById('versionName') as HTMLInputElement).classList.add('is-invalid');
        }
        if (levelId === '' || levelTitle === '' || versionName === '') {
            return;
        }

        // Send the POST request
        fetch(import.meta.env.VITE_BACKEND_URL + 'api/initLevel.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ levelId, levelTitle, levelDescription, versionName }),
            credentials: 'include'
        })
            .then(response => response.json())
            .then(data => {
                if (data.status === 'success') {
                    deleteCache(levelId).then((data) => {
                        if (data.status === 'success') {
                            window.location.href = window.location.href + '?notification=success&message=Le+niveau+a+bien+%C3%A9t%C3%A9+initialis%C3%A9.';
                        } else {
                            toasts.show({
                                headerContent: 'Erreur',
                                bodyContent: 'Une erreur est survenue lors de la suppression du cache.',
                                toastProps: {
                                    bg: 'danger',
                                    autohide: true,
                                    delay: 3000,
                                },
                            });
                        }
                    });
                } else if (data.status === 'level_already_exists') {
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'L\'identifiant du niveau existe déjà.',
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 3000,
                        },
                    });
                } else if (data.status === 'missing_fields') {
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'Veuillez remplir tous les champs.',
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 3000,
                        },
                    });


                }
            })
            .catch((error) => {
                console.error('Error:', error);
                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: 'Une erreur est survenue lors de l\'initialisation du niveau.',
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 3000,
                    },
                });
            });
    }

    return (
        <Alert variant="warning" className='mt-3'>
            <FontAwesomeIcon icon="exclamation-triangle" className="me-2" /> <b>Attention</b> : le niveau n'est pas initialisé.

            {/* Level ID */}
            <Form.Group className='mt-3'>
                <Form.Label>Identifiant du niveau</Form.Label>
                <Form.Control
                    placeholder="Identifiant du niveau"
                    aria-label="Identifiant du niveau"
                    aria-describedby="levelId"
                    id="levelId"
                    value={level}
                    required
                />
            </Form.Group>

            {/* Level title */}
            <Form.Group className='mt-3'>
                <Form.Label>Titre du niveau</Form.Label>
                <Form.Control placeholder="Titre du niveau" id="levelTitle" required />
            </Form.Group>

            {/* Level description */}
            <Form.Group className='mt-3'>
                <Form.Label>Description du niveau</Form.Label>
                <Form.Control placeholder="Description du niveau" id="levelDescription" />
            </Form.Group>

            {/* Version name */}
            <Form.Group className='mt-3'>
                <Form.Label>Nom de la première version (<i className="small">ex. Année scolaire en cours : 2024-2025</i>) </Form.Label>
                <Form.Control placeholder="Nom de la version" id="versionName" required />
            </Form.Group>

            {/* Submit button */}
            <Form.Group className='mt-3'>
                <Button type="submit" variant="warning" onClick={() => {
                    handleLevelInit();
                }}>
                    <FontAwesomeIcon icon="check" className="me-2" /> Initialiser le niveau
                </Button>
            </Form.Group>
        </Alert>

    )
}

export default LevelInit;
