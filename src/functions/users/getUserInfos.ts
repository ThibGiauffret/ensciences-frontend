// Get the user infos from the backend

export async function getUserInfos() {
    const backendUrl = import.meta.env.VITE_BACKEND_URL;
    try {
        const response = await fetch(backendUrl + 'api/userCheck.php', {
            method: 'GET',
            credentials: 'include'
        });
        const data = await response.json();
        if (data.status === 'success') {
            return { 'data': data };
        }
    } catch (error) {
        console.error('Erreur lors de la récupération des informations de l\'utilisateur:', error);
        return { 'error': "Erreur lors de la récupération des informations de l'utilisateur." };
    }
}

export default getUserInfos;
