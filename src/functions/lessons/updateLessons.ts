
interface Response {
    status: 'success' | 'danger';
    header: string;
    body: string;
}

// Return the response after updating the lessons
async function updateLessons(level: string, version: string, rawLessons: Lessons): Promise<Response> {
    return uploadLessons(level, version, rawLessons).then((data: any): Response => {
        if (data.status === 'success') {
            return { status: 'success', header: 'Succès', body: 'Les leçons ont été enregistrées avec succès.' };
        } else if (data.status === 'not_authorized') {
            return { status: 'danger', header: 'Erreur', body: 'Vous n\'êtes pas autorisé à enregistrer les leçons.' };
        } else if (data.status === 'data_file_not_found') {
            return { status: 'danger', header: 'Erreur', body: 'Le fichier de données n\'a pas été trouvé.' };
        } else {
            return { status: 'danger', header: 'Erreur', body: 'Une erreur est survenue lors de l\'enregistrement des leçons.' };
        }
    });
}

// Send the lessons data to the backend
async function uploadLessons(level: string, version: string, data: any) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_BACKEND_URL;
    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'api/uploadLessons.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ level, version, data }),
            credentials: 'include'
        });
        // Get the response
        const responseData = await response.json();
        if (responseData.status === 'success') {
            return { "status": "success" };
        } else {
            return { 'status': responseData.status };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { updateLessons };