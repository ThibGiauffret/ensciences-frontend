async function getFile(level: string, version: string, file: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_BACKEND_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'api/getFile.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ level, version, file }),
            credentials: 'include'
        });

        // Get the response
        const data = await response;
        // Check if the response is file or json
        if (data.headers.get('Content-Type')?.includes('application/json')) {
            const newdata = await data.json();
            return { 'status': newdata.status };
        } else {
            // If the response is a file, get the file extension from filename
            const ext = file.split('.').pop();
            // Get the file as a blob
            const fileBlob = await data.blob();
            // Create a URL for the file
            const fileUrl = URL.createObjectURL(fileBlob);
            return { 'status': 'success', 'ext': ext, 'file': fileUrl };
        }

    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { getFile };