// ----------------------------
// Card components
// ----------------------------

import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Popover from 'react-bootstrap/Popover';
import { Col, Row, Placeholder } from "react-bootstrap";
import ScrollAnimation from "react-animate-on-scroll";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import { ButtonLink } from "./buttons";

import styles from "./css/cards.module.css";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { IconProp, library } from '@fortawesome/fontawesome-svg-core';
library.add(fas);

// Simple card for blog posts (used in the home page)
function SimpleBlogCard(props: { title: string, author: string, date: string, href: string, children: React.ReactNode }) {
  return (
    <Card className={styles.card + " mb-3"}>
      <Card.Body>
        <Card.Title>{props.title}</Card.Title>
        <Card.Text>
          {props.children}
        </Card.Text>
      </Card.Body>

      <Card.Footer className={styles.cardFooter}>
        <span className="small me-3">
          <FontAwesomeIcon icon="user"></FontAwesomeIcon>
          &nbsp;{props.author}</span>
        <span className="small me-3">
          <FontAwesomeIcon icon="calendar-alt"></FontAwesomeIcon>
          &nbsp;{props.date}</span>
        <ButtonLink href={props.href} className={styles.cardButton}>
          <FontAwesomeIcon icon="eye"></FontAwesomeIcon>&nbsp;Consulter
        </ButtonLink>
      </Card.Footer>
    </Card>
  );
}

function LoginCard({ children, ...props }: { children: React.ReactNode, className?: string }) {
  const childrenArray = React.Children.toArray(children);

  const [activeTab, setActiveTab] = React.useState('login')

  const handleTabClick = (tabName: string) => {
    setActiveTab(tabName);
  };

  return (
    <Card className={styles.card + " mb-3" + (props.className ? ' ' + props.className : '')}>
      {/* Card Header with two tabs */}
      <Card.Header>
        <ul className="nav nav-tabs card-header-tabs">
          <li className="nav-item">
            <a className={"nav-link " + (activeTab.toString() === 'login' ? 'active' : '')} onClick={() => handleTabClick('login')} id="login-tab"
            >Connexion</a>
          </li>
          <li className="nav-item">
            <a className={"nav-link " + (activeTab.toString() === 'register' ? 'active' : '')} onClick={() => handleTabClick('register')} id="register-tab"
            >Inscription</a>
          </li>
        </ul>
      </Card.Header>

      {/* Card Body */}
      <Card.Body>
        <div className="tab-content">
          <div className={"tab-pane " + (activeTab.toString() === 'login' ? 'active' : '')} id="login">
            {React.Children.count(childrenArray) > 0 && childrenArray[0]}
          </div>
          <div className={"tab-pane " + (activeTab.toString() === 'register' ? 'active' : '')} id="register">
            {React.Children.count(childrenArray) > 1 && childrenArray[1]}
          </div>
        </div>
      </Card.Body>
    </Card>
  );
}

function ThemeCard(props: { name: string, title: string, icon: IconProp, color: string, children: React.ReactNode }) {

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // If all fields are empty, return an empty card with placeholders
  if (props.name === "") {
    // Placeholder card
    return (
      <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
        <Card className={styles.card + " " + styles.themeCard + " h-100 mx-auto"}>
          <div className={styles.themeCardHeader} style={{ backgroundColor: "#555" }} >
          </div>
          <Card.Body className="text-center">
            <Placeholder as={Card.Title} animation="glow">
              <Placeholder xs={8} /><br />
              <Placeholder xs={4} />
            </Placeholder>
          </Card.Body>
          <Card.Footer className="text-center">
            <Placeholder bg="primary" xs={6} />
          </Card.Footer>
        </Card>
      </ScrollAnimation>
    );
  } else {
    // Card with icon, title, color and children
    return (
      <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20} className="h-100 mx-auto">
        <Card className={styles.card + " " + styles.themeCard + " h-100 mx-auto"}>
          {/* A div with "color" background and icon at the center */}
          <div className={styles.themeCardHeader} style={{ backgroundColor: props.color, color:"white" }}>
            <FontAwesomeIcon icon={props.icon} size="5x" />
          </div>
          <Card.Body className="d-flex flex-column justify-content-center align-items-center text-center">
            <Card.Title>{props.title}</Card.Title>
          </Card.Body>
          <Card.Footer className="text-center">
            <ButtonLink className={styles.cardButton + " stretched-link fw-bold"} onClick={handleShow}>
              <FontAwesomeIcon icon="eye"></FontAwesomeIcon>&nbsp;Consulter
            </ButtonLink >
          </Card.Footer>

          {/* Modal */}
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>
                <FontAwesomeIcon icon={props.icon} className="me-2" />
                {props.title}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {props.children}

            </Modal.Body>
          </Modal>
        </Card>
      </ScrollAnimation>
    );
  }
}

function FileCard(props: { id: string, title: string, icon: IconProp, description: string, buttons: any }) {
  // If all fields are empty, return an empty card with placeholders
  if (props.title !== "") {
    // Card with icon, title, description and buttons
    return (
      <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
        <Card className={styles.card + " my-3 p-3"}>
          <Row>
            <Col xs={12} sm={8} className="d-flex flex-row align-items-center">
              <FontAwesomeIcon icon={props.icon} className="text-center ms-2 me-4" style={{ fontSize: "1.5rem" }} />
              <div className="text-start">
                <span className="fw-bold">{props.title}</span><br />
                <span className="small">{props.description}</span>
              </div>
            </Col>
            <Col xs={12} sm={4} className="d-flex flex-row align-items-center justify-content-end">
              <div className="d-flex flex-wrap justify-content-end">
                {props.buttons && props.buttons.map((button: any) => {
                  return (
                    <OverlayTrigger key={props.id + " " + button.icon} overlay={<Popover>
                      <Popover.Body>
                        <b>{button.name}</b><br />
                        Date : {new Date(button.date*1000).toLocaleDateString("fr-FR", { year: 'numeric', month: 'long', day: 'numeric' })}<br />
                        Taille : {(button.size / 1024 / 1024).toFixed(2)} Mo<br />
                        
                      </Popover.Body>
                  </Popover>}>
                      <Button className={"btn-sm m-1 btn-" + button.color} href={button.path}
                      target="_blank">
                        <FontAwesomeIcon icon={button.icon}
                          fixedWidth={true} />
                        {button.text}
                      </Button>
                    </OverlayTrigger>
                  );
                })}
              </div>
            </Col>
          </Row>
        </Card>
      </ScrollAnimation>
    );
  } else {
    // Placeholder card
    return (
      <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
        <Card className={styles.card + " my-3"}>
          <Row>
            <Col xs={12} sm={8} className="d-flex flex-row align-items-center">
              <Placeholder xs={1} className="mx-4" />
              <div className="my-3 w-100">
                <Placeholder xs={8} /><br />
                <Placeholder xs={6} />
              </div>
            </Col>
            <Col xs={12} sm={4} className="d-flex flex-row justify-content-end align-items-center">
              <div className="d-flex flex-row justify-content-end me-3">
                <Placeholder.Button xs={6} className="btn-sm m-1" variant="primary" />
                <Placeholder.Button xs={6} variant="danger" className="btn-sm m-1" />
              </div>
            </Col>
          </Row>
        </Card>
      </ScrollAnimation>

    );
  }
}

export { SimpleBlogCard, LoginCard, ThemeCard, FileCard };