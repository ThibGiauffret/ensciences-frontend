// A notification component that displays a toast message based on the URL parameters

import { useEffect, useRef } from 'react';
import { useToasts } from 'react-bootstrap-toasts';
import removeParam from '../functions/url/removeParam';

function Notification() {
    const urlParams = new URLSearchParams(window.location.search);
    const notification = urlParams.get('notification');
    const message = urlParams.get('message');

    // Appel du hook useToasts à l'intérieur du corps du composant fonctionnel
    const toasts = useToasts();

    // À l'intérieur de votre composant
    const notificationPrev = useRef<string | null>(null);
    const messagePrev = useRef<string | null>(null);

    useEffect(() => {
        // Vérifie si les valeurs ont changé
        if (notification !== notificationPrev.current || message !== messagePrev.current) {
            notificationPrev.current = notification;
            messagePrev.current = message;

            if (notification !== null || message !== null) {
                switch (notification) {
                    case 'success':
                        toasts.show({
                            headerContent: 'Succès',
                            bodyContent: message || '',
                            toastProps: {
                                bg: 'success',
                                autohide: true,
                                delay: 3000,
                            },
                        });
                        break;
                    case 'error':
                        toasts.show({
                            headerContent: 'Erreur',
                            bodyContent: message || '',
                            toastProps: {
                                bg: 'danger',
                                autohide: true,
                                delay: 3000,
                            },
                        });
                        break;
                    case 'info':
                        toasts.show({
                            headerContent: 'Information',
                            bodyContent: message || '',
                            toastProps: {
                                bg: 'info',
                                autohide: true,
                                delay: 3000,
                            },
                        });
                        break;
                    // Ajoutez d'autres cas au besoin
                }
            }
        }
        // Incluez toutes les dépendances externes utilisées dans useEffect
    }, []);

    // Remove the notification parameter from the URL
    removeParam(window.location.href, ['notification', 'message']);

    return null
}

export default Notification;