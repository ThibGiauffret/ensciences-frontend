// ----------------------------
// Seconde PC page
// ----------------------------

// Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { H2 } from '../components/sections'
import { PageContainer, FullContainer } from '../components/containers';
import Dropdown from 'react-bootstrap/Dropdown';
import { Alert, Button } from 'react-bootstrap';
import { LessonsFileTable, LessonsThemes, LessonsAdditionalFiles } from '../components/lessons/lessons';
import LessonsEditor from '../components/lessons/lessonsEditor';
import FilesManager from '../components/lessons/filesManager';
import LevelInit from '../components/lessons/levelInit';

// Functions
import { useState, useEffect } from 'react';
import { getLessons } from '../functions/lessons/getLessons';
import getUserInfos from '../functions/users/getUserInfos';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "animate.css/animate.compat.css"
library.add(fas)
library.add(fab)



function SecondePC(props: { title: string }) {

    const level = 'seconde_pc';

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
            const header = document.getElementById('pageHeader');
            if (header) {
                header.innerHTML = props.title;
            }
        };
        setTitle();
    }, []);

    // Set the different states
    const [lessonsThemes, setLessonsThemes] = useState<any>([]);
    const [lessonsFileTable, setLessonsFileTable] = useState([]);
    const [versionList, setVersionList] = useState([{ version: '' }]);
    const [currentVersion, setCurrentVersion] = useState('');
    const [newLesson, setNewLesson] = useState(false);
    const [missingVersion, setMissingVersion] = useState(false);
    const [userIsAdmin, setUserIsAdmin] = useState(false);

    // Get the version from the url and get the lessons data
    const urlVersion = new URLSearchParams(window.location.search).get('version');
    useEffect(() => {
        getLessons(level).then((lessonsData) => {
            if (lessonsData) {
                if (lessonsData.status === 'success') {

                    // Extract the version list
                    const versions: { version: string }[] = [];
                    lessonsData.lessons.versions.forEach((version: any) => {
                        versions.push({ version: version.version });
                    });
                    // If versionList length is greater than 1, set the version list
                    if (versions.length > 1) {
                        setVersionList(versions);
                    } else {
                        // Hide the version dropdown if there is only one version
                        const versionDropdown = document.getElementById('versionList');
                        if (versionDropdown) {
                            versionDropdown.classList.add('d-none');
                        }
                    }

                    // Select the lessons version according to the current year (in url param)
                    if (urlVersion) {
                        const currentLessons = lessonsData.lessons.versions.filter((lesson: any) => lesson.version.toString() === urlVersion)[0];
                        // Add connected status to currentLessons
                        if (currentLessons) {
                            currentLessons.connected = lessonsData.connected;
                            // Update the lessons themes and the lessons file table
                            setLessonsThemes(currentLessons);
                            setLessonsFileTable(currentLessons);
                            setCurrentVersion(currentLessons.version);
                        } else {
                            // If the version is not found, display the latest version
                            const latestVersion = lessonsData.lessons.versions[lessonsData.lessons.versions.length - 1];
                            // Add connected status to currentLessons
                            latestVersion.connected = lessonsData.connected;
                            // Update the lessons themes and the lessons file table
                            setLessonsThemes(latestVersion);
                            setLessonsFileTable(latestVersion);
                            setCurrentVersion(latestVersion.version);
                            // Remove version from the url
                            const currentUrl = new URL(window.location.href);
                            currentUrl.searchParams.delete('version');
                            window.history.pushState({}, '', currentUrl.toString());
                        }

                    } else {
                        // If no version is selected, display the latest version
                        const latestVersion = lessonsData.lessons.versions[lessonsData.lessons.versions.length - 1];
                        if (latestVersion) {
                            // Add connected status to currentLessons
                            latestVersion.connected = lessonsData.connected;
                            // Update the lessons themes and the lessons file table
                            setLessonsThemes(latestVersion);
                            setLessonsFileTable(latestVersion);
                            setCurrentVersion(latestVersion.version);
                        } else {
                            setMissingVersion(true);
                        }
                    }
                } else if (lessonsData.status === "no_lessons") {
                    setNewLesson(true);
                }
            }
        });

        // Get user infos
        getUserInfos().then((response: any) => {
            if (response.data) {
              if (response.data.role === 'administrator') {
                setUserIsAdmin(true);
              }
            }
          });
    }, []);




    return (
        <PageContainer>
            <FullContainer>
                <Alert variant="info" className='mt-3'>
                    <FontAwesomeIcon icon="person-digging" className="me-2" /> <b>À l'attention des collègues</b> : sans plus d&apos;informations sur la façon dont ils sont mis en oeuvre, <u>les documents de cette page ne peuvent être proposés en classe sans une phase d'appropriation et d'adaption</u>. Vous trouverez des choses à reprendre, je suis preneur de toute remarque ou correction <a href="/contact">ici</a>.
                </Alert>

                {missingVersion ? (
                    <Alert variant="danger" className='mt-3'>
                        <FontAwesomeIcon icon="exclamation-triangle" className="me-2" /> Aucune version du niveau n'est configurée pour cette page. Cliquer sur le bouton "Gérer la page" puis "Éditer le niveaux et les versions" pour ajouter une version.
                    </Alert>
                ) : null}

                {newLesson ? (

                    // Initialize level
                    <LevelInit level={level} />

                ) : (
                    <>
                        {userIsAdmin ? (
                            <Dropdown id="adminDropdown" className='d-inline-block'>
                                <Dropdown.Toggle variant="secondary" id="adminDropdown" className='btn-sm btn-warning'>
                                    <FontAwesomeIcon icon="tools" />&nbsp;Gérer la page
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    {/* Versions editor */}
                                    <LessonsEditor level={level} version={currentVersion} editor="versions" />

                                    {/* Themes Editor */}
                                    <LessonsEditor level={level} version={currentVersion} editor="themes" />

                                    {/* Additional Files Editor */}
                                    <LessonsEditor level={level} version={currentVersion} editor="additional_files" />

                                    {/* Files Manager */}
                                    <FilesManager level={level} version={currentVersion} />
                                </Dropdown.Menu>
                            </Dropdown>
                        ) : null}
                        <div className='d-flex justify-content-between align-items-center'>
                            <H2>Les documents par thème</H2>
                            <span>
                                {/* Search button */}
                                <Button variant="secondary" className='btn-sm me-2'
                                    // Onclick, scroll to the search bar #searchFiles
                                    onClick={() => {
                                        const searchFiles = document.getElementById('searchFiles');
                                        if (searchFiles) {
                                            searchFiles.scrollIntoView({ behavior: 'smooth' });
                                            // Focus on the search bar
                                            const searchInput = document.getElementById('searchInput');
                                            if (searchInput) {
                                                searchInput.focus();
                                            }
                                        }
                                    }}>
                                    <FontAwesomeIcon icon="search" />
                                </Button>
                                {/* Version history dropdown */}
                                <Dropdown className='d-inline-block' id="versionList">
                                    <Dropdown.Toggle variant="secondary" id="changeVersion" className='btn-sm'>
                                        <FontAwesomeIcon icon="history" className="me-2" /> {lessonsThemes.version}
                                    </Dropdown.Toggle>

                                    <Dropdown.Menu>
                                        {versionList.map((version: any) => {
                                            return (
                                                <Dropdown.Item href={"/seconde_pc?version=" + version.version} key={version.version}>{version.version}
                                                </Dropdown.Item>
                                            );
                                        })}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </span>
                        </div>

                        {/* Theme cards */}
                        <LessonsThemes lessons={lessonsThemes} level={level} levelVersion={currentVersion}/>

                        <Row>
                            <Col xs={12} md={12} lg={8}>
                                <H2>Liste des documents</H2>

                                {/* Lessons file table */}
                                <LessonsFileTable lessons={lessonsFileTable} level={level} levelVersion={currentVersion}/>
                            </Col>
                            <Col xs={12} md={12} lg={4}>
                                {/* Additional files */}
                                <LessonsAdditionalFiles lessons={lessonsFileTable} />
                            </Col>
                        </Row>
                    </>
                )}
            </FullContainer>
        </PageContainer>
    );
}

export default SecondePC;