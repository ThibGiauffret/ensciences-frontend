import path from 'node:path';
import { createRequire } from 'node:module';

import { defineConfig, normalizePath } from 'vite';
import { viteStaticCopy } from 'vite-plugin-static-copy';

const require = createRequire(import.meta.url);
const cMapsDir = normalizePath(
  path.join(path.dirname(require.resolve('pdfjs-dist/package.json')), 'cmaps'),
);
const standardFontsDir = normalizePath(
  path.join(path.dirname(require.resolve('pdfjs-dist/package.json')), 'standard_fonts'),
);
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => ({
  plugins: [react(),
  viteStaticCopy({
    targets: [
      {
        src: cMapsDir,
        dest: '',
      },
      { src: standardFontsDir, dest: '' },
    ],
  }),
  ],
  // Define the environment variable BACKEND_URL (in .env and .env.production)
  define: {
    'process.env.BACKEND_URL': mode === 'production' ? process.env.VITE_BACKEND_URL : process.env.VITE_BACKEND_URL,
  }
}))