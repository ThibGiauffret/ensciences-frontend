interface FilesManagerFiles {
    uuid: string;
    name: string;
    dir: string;
    path: string;
    size: number;
    date: number;
    ext: string;
}