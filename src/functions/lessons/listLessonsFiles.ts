async function listLessonsFiles(level: string, version: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_BACKEND_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'api/functions/listLessonsFiles.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ level, version }),
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            return { "status": "success", "message": data.message, "files": data.files, "folders": data.folders };
        } else {
            return { 'status': data.status, 'message': data.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { listLessonsFiles };