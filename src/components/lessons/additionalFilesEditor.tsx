// Components
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { CollapsibleAlert } from '../collapsibleAlert';
import IconSelector from '../iconSelector';
import { Button } from 'react-bootstrap';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

// CSS
import styles from '../css/cards.module.css';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useEffect, useState, useRef } from 'react';

// Additional files editor card (modal content)
function AdditionalFilesEditorContent({ version, json, setJson, setModified }: { version: string, json: Lessons, setJson: any, setModified: any }) {

    let level = json.id;
    let levelVersion = version;

    const isFirstRender = useRef(true);

    // Filter the json.version and keep only the version we want to edit
    const versionData = json.versions.filter((v: Version) => v.version === version);
    const [sections, setSections] = useState(versionData[0].additional_files);
    const [availableAdditionalFiles] = useState(versionData[0].availableAdditionalFiles);

    // Send the updated sections to the parent component when the sections are updated
    useEffect(() => {
        // Do nothing on the first render
        if (isFirstRender.current) {
            isFirstRender.current = false;
            return;
        }

        setJson({
            ...json, versions: json.versions.map((v: Version) => {
                if (v.version === version) {
                    return { ...v, additional_files: sections };
                }
                return v;
            })
        });
        setModified(true);
    }, [sections]);

    // Handle the tab click
    const [activeTab, setActiveTab] = useState(sections.length > 0 ? sections[0].id : 0);
    const handleTabClick = (tabId: number) => {
        setActiveTab(tabId);
    };

    // Add a new section
    const handleAddSection = () => {
        const newSections = addSection([...sections]);
        let newSectionId = 0;
        if (newSections.length > 0) {
            newSectionId = newSections[newSections.length - 1].id;
        }
        setSections(newSections);
        setActiveTab(newSectionId);
    };

    return (
        <Card className={styles.card + " mb-3"} key={sections.length}>
            <Card.Header>
                <ul className="nav nav-tabs card-header-tabs">
                    {sections && sections.map((section: AdditionalFile) => (
                        <li key={section.id + '_section_nav'} className="nav-item">
                            <a onClick={() => {
                                handleTabClick(section.id)
                            }} className={"nav-link " + (
                                activeTab === section.id ? 'active' : ''
                            )}>
                                {section.section}
                            </a>
                        </li>
                    ))}

                    {/* Add section button */}
                    <li className="nav-item">
                        <a className='nav-link' onClick={handleAddSection}>
                            <FontAwesomeIcon icon="plus" />
                        </a>
                    </li>
                </ul>
            </Card.Header>

            {/* Card Body */}
            <Card.Body>
                <div className="tab-content">
                    {sections && sections.map((section: AdditionalFile) => (
                        <div key={section.id + '_section_tab'} className={"tab-pane " + (
                            activeTab === section.id ? 'active' : ''
                        )} id={section.section}>

                            <div key={section.id + "_section_content"}>
                                <h4>Paramétrage de la section</h4>
                                {/* Section parameters */}
                                <InputGroup className="mb-3" size="sm">
                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Titre de la section.</Tooltip>}>
                                        <InputGroup.Text id="theme_id">
                                            <FontAwesomeIcon icon="font" />
                                        </InputGroup.Text>
                                    </OverlayTrigger>
                                    <Form.Control
                                        placeholder="Identifiant"
                                        aria-label="Identifiant"
                                        aria-describedby="section_id"
                                        value={section.section}
                                        onChange={(e) => {
                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                if (s.id === section.id) {
                                                    return { ...s, section: e.target.value };
                                                }
                                                return s;
                                            });
                                            setSections(updatedSections);
                                        }}
                                    />
                                    {/* Delete section button */}
                                    <Button variant="danger" size="sm" onClick={() => {
                                        if (confirm("Voulez-vous vraiment supprimer cette section ?")) {
                                            const updatedSections = sections.filter((s: AdditionalFile) => s.id !== section.id);
                                            setSections(updatedSections);
                                            setActiveTab(updatedSections[updatedSections.length - 1].id);
                                        }
                                    }}>
                                        <FontAwesomeIcon icon="trash" />
                                    </Button>
                                </InputGroup>

                                <hr />

                                <h4>Fichiers</h4>

                                {/* File list */}
                                {section.files && section.files.map((file: AdditionalFileFiles) => (
                                    <>
                                        <CollapsibleAlert key={"section_" + section.id + "_file_" + file.id} title={file.title} variant="secondary">
                                            <div className="mt-3">
                                                {/* Title */}
                                                <InputGroup size="sm" className="mb-3">
                                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Titre du fichier</Tooltip>}>
                                                        <InputGroup.Text id="file_title">
                                                            <FontAwesomeIcon icon="font" />
                                                        </InputGroup.Text>
                                                    </OverlayTrigger>
                                                    <Form.Control
                                                        placeholder="Titre"
                                                        aria-label="Titre"
                                                        aria-describedby="file_title"
                                                        value={file.title}
                                                        onChange={(e) => {
                                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                                if (s.id === section.id) {
                                                                    const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                        if (f.id === file.id) {
                                                                            return { ...f, title: e.target.value };
                                                                        }
                                                                        return f;
                                                                    });
                                                                    return { ...s, files: updatedFiles };
                                                                }
                                                                return s;
                                                            });
                                                            setSections(updatedSections);
                                                        }}
                                                    />
                                                    <Button variant="danger" size="sm" onClick={() => {
                                                        if (confirm("Voulez-vous vraiment supprimer ce fichier ?")) {
                                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                                if (s.id === section.id) {
                                                                    const updatedFiles = s.files.filter((f: AdditionalFileFiles) => f.id !== file.id);
                                                                    return { ...s, files: updatedFiles };
                                                                }
                                                                return s;
                                                            });
                                                            setSections(updatedSections);
                                                        }
                                                    }}>
                                                        <FontAwesomeIcon icon="trash" />
                                                    </Button>
                                                </InputGroup>
                                                {/* Description */}
                                                <InputGroup size="sm" className="mb-3">
                                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Description du fichier</Tooltip>}>
                                                        <InputGroup.Text id="file_description">
                                                            <FontAwesomeIcon icon="info" />
                                                        </InputGroup.Text>
                                                    </OverlayTrigger>
                                                    <Form.Control
                                                        placeholder="Description"
                                                        aria-label="Description"
                                                        aria-describedby="file_description"
                                                        value={file.description}
                                                        onChange={(e) => {
                                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                                if (s.id === section.id) {
                                                                    const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                        if (f.id === file.id) {
                                                                            return { ...f, description: e.target.value };
                                                                        }
                                                                        return f;
                                                                    });
                                                                    return { ...s, files: updatedFiles };
                                                                }
                                                                return s;
                                                            });
                                                            setSections(updatedSections);
                                                        }}
                                                    />
                                                </InputGroup>
                                                {/* Icon */}
                                                <InputGroup size="sm" className="mb-3">
                                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Icône du fichier</Tooltip>}>
                                                        <InputGroup.Text id="file_icon">
                                                            <FontAwesomeIcon icon="star" />
                                                        </InputGroup.Text>
                                                    </OverlayTrigger>
                                                    <IconSelector value={file.icon} setValue={(icon: string) => {
                                                        const updatedSections = sections.map((s: AdditionalFile) => {
                                                            if (s.id === section.id) {
                                                                const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                    if (f.id === file.id) {
                                                                        return { ...f, icon: icon };
                                                                    }
                                                                    return f;
                                                                });
                                                                return { ...s, files: updatedFiles };
                                                            }
                                                            return s;
                                                        }
                                                        );
                                                        setSections(updatedSections);
                                                    }
                                                    } />
                                                </InputGroup>

                                                {/* Buttons */}
                                                <h5>Boutons</h5>
                                                <div className="mt-3">
                                                    {file.buttons && file.buttons.map((button: Button) => (
                                                        <CollapsibleAlert key={"section_" + section.id + "_file_" + file.id + "_button_" + button.id} variant="secondary" title={button.title}>
                                                            <div className="mt-3">
                                                                {/* Title */}
                                                                <InputGroup size="sm" className="mb-3">
                                                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Texte du bouton</Tooltip>}>
                                                                        <InputGroup.Text id="button_text">
                                                                            <FontAwesomeIcon icon="font" />
                                                                        </InputGroup.Text>
                                                                    </OverlayTrigger>
                                                                    <Form.Control
                                                                        placeholder="Texte"
                                                                        aria-label="Texte"
                                                                        aria-describedby="button_text"
                                                                        value={button.title}
                                                                        onChange={(e) => {
                                                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                                                if (s.id === section.id) {
                                                                                    const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                        if (f.id === file.id) {
                                                                                            const updatedButtons = f.buttons.map((b: Button) => {
                                                                                                if (b.id === button.id) {
                                                                                                    return { ...b, title: e.target.value };
                                                                                                }
                                                                                                return b;
                                                                                            });
                                                                                            return { ...f, buttons: updatedButtons };
                                                                                        }
                                                                                        return f;
                                                                                    });
                                                                                    return { ...s, files: updatedFiles };
                                                                                }
                                                                                return s;
                                                                            });
                                                                            setSections(updatedSections);
                                                                        }}
                                                                    />
                                                                    <Button variant="danger" size="sm" onClick={() => {
                                                                        if (confirm("Voulez-vous vraiment supprimer ce bouton ?")) {
                                                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                                                if (s.id === section.id) {
                                                                                    const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                        if (f.id === file.id) {
                                                                                            const updatedButtons = f.buttons.filter((b: Button) => b.id !== button.id);
                                                                                            return { ...f, buttons: updatedButtons };
                                                                                        }
                                                                                        return f;
                                                                                    });
                                                                                    return { ...s, files: updatedFiles };
                                                                                }
                                                                                return s;
                                                                            });
                                                                            setSections(updatedSections);
                                                                        }
                                                                    }}>
                                                                        <FontAwesomeIcon icon="trash" />
                                                                    </Button>
                                                                </InputGroup>
                                                                {/* Icon */}
                                                                <InputGroup size="sm" className="mb-3">
                                                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Icône du bouton</Tooltip>}>
                                                                        <InputGroup.Text id="button_icon">
                                                                            <FontAwesomeIcon icon="star" />
                                                                        </InputGroup.Text>
                                                                    </OverlayTrigger>
                                                                    <IconSelector value={button.icon} setValue={(icon: string) => {
                                                                        const updatedSections = sections.map((s: AdditionalFile) => {
                                                                            if (s.id === section.id) {
                                                                                const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                    if (f.id === file.id) {
                                                                                        const updatedButtons = f.buttons.map((b: Button) => {
                                                                                            if (b.id === button.id) {
                                                                                                return { ...b, icon: icon };
                                                                                            }
                                                                                            return b;
                                                                                        });
                                                                                        return { ...f, buttons: updatedButtons };
                                                                                    }
                                                                                    return f;
                                                                                });
                                                                                return { ...s, files: updatedFiles };
                                                                            }
                                                                            return s;
                                                                        }
                                                                        );
                                                                        setSections(updatedSections);
                                                                    }} />
                                                                </InputGroup>
                                                                {/* Color select */}
                                                                <InputGroup size="sm" className="mb-3">
                                                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Couleur du bouton</Tooltip>}>
                                                                        <InputGroup.Text id="button_color">
                                                                            <FontAwesomeIcon icon="palette" />
                                                                        </InputGroup.Text>
                                                                    </OverlayTrigger>
                                                                    <Form.Select
                                                                        aria-label="Couleur"
                                                                        aria-describedby="button_color"
                                                                        value={button.color}
                                                                        onChange={(e) => {
                                                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                                                if (s.id === section.id) {
                                                                                    const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                        if (f.id === file.id) {
                                                                                            const updatedButtons = f.buttons.map((b: Button) => {
                                                                                                if (b.id === button.id) {
                                                                                                    return { ...b, color: e.target.value };
                                                                                                }
                                                                                                return b;
                                                                                            });
                                                                                            return { ...f, buttons: updatedButtons };
                                                                                        }
                                                                                        return f;
                                                                                    });
                                                                                    return { ...s, files: updatedFiles };
                                                                                }
                                                                                return s;
                                                                            });
                                                                            setSections(updatedSections);
                                                                        }}
                                                                    >
                                                                        <option value="primary">
                                                                            <span className="bg-primary">Primaire</span></option>
                                                                        <option value="secondary">
                                                                            <span className="text-secondary">Secondaire</span></option>
                                                                        <option value="success">
                                                                            <span className="text-success">Succès</span></option>
                                                                        <option value="danger">
                                                                            <span className="text-danger">Danger</span></option>
                                                                        <option value="warning">
                                                                            <span className="text-warning">Attention</span></option>
                                                                        <option value="info">
                                                                            <span className="text-info">Info</span></option>
                                                                        <option value="light">
                                                                            <span className="text-light">Clair</span></option>
                                                                        <option value="dark">
                                                                            <span className="text-dark">Sombre</span></option>
                                                                    </Form.Select>
                                                                </InputGroup>
                                                                {/* File selection select */}
                                                                <InputGroup size="sm" className="mb-3">
                                                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Fichier associé au bouton</Tooltip>}>
                                                                        <InputGroup.Text id="button_file">
                                                                            <FontAwesomeIcon icon="file" />
                                                                        </InputGroup.Text>
                                                                    </OverlayTrigger>
                                                                    <Form.Select
                                                                        aria-label="Fichier"
                                                                        aria-describedby="button_file"
                                                                        value={button.name}
                                                                        onChange={(e) => {
                                                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                                                if (s.id === section.id) {
                                                                                    const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                        if (f.id === file.id) {
                                                                                            const updatedButtons = f.buttons.map((b: Button) => {
                                                                                                if (b.id === button.id) {
                                                                                                    return {
                                                                                                        ...b, path: "/view?level=" + level + "&version=" + levelVersion + "&file=additional_files/" + e.target.value,
                                                                                                        date: parseInt(e.target.options[e.target.selectedIndex].getAttribute('data-date') || '0'),
                                                                                                        size: parseInt(e.target.options[e.target.selectedIndex].getAttribute('data-size') || '0'),
                                                                                                        name: e.target.options[e.target.selectedIndex].text
                                                                                                    };
                                                                                                }
                                                                                                return b;
                                                                                            });
                                                                                            return { ...f, buttons: updatedButtons };
                                                                                        }
                                                                                        return f;
                                                                                    });
                                                                                    return { ...s, files: updatedFiles };
                                                                                }
                                                                                return s;
                                                                            });
                                                                            setSections(updatedSections);
                                                                        }}
                                                                    >
                                                                        <option value="">Sélectionner un fichier</option>
                                                                        {availableAdditionalFiles && availableAdditionalFiles.map((s: any) => (
                                                                            <option value={s.name} data-date={s.date} data-size={s.size}>{s.name}</option>

                                                                        ))}
                                                                    </Form.Select>
                                                                </InputGroup>
                                                            </div>
                                                        </CollapsibleAlert>
                                                    ))}
                                                    <Button variant="success" size="sm" className="m-1" onClick={() => {
                                                        const updatedSections = sections.map((s: AdditionalFile) => {
                                                            if (s.id === section.id) {
                                                                const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                    if (f.id === file.id) {
                                                                        const newButton = {
                                                                            id: file.buttons.length,
                                                                            icon: "",
                                                                            date: 0,
                                                                            color: "",
                                                                            name: "",
                                                                            path: "",
                                                                            size: 0,
                                                                            title: ""
                                                                        };
                                                                        f.buttons.push(newButton);
                                                                        return f;
                                                                    }
                                                                    return f;
                                                                });
                                                                return { ...s, files: updatedFiles };
                                                            }
                                                            return s;
                                                        });
                                                        setSections(updatedSections);
                                                    }}>
                                                        <FontAwesomeIcon icon="plus" />&nbsp;Ajouter un bouton
                                                    </Button>
                                                </div>
                                            </div>
                                        </CollapsibleAlert>
                                    </>
                                ))}

                                {/* Add file button */}
                                <Button variant="success" size="sm" className="mt-3" onClick={() => {
                                    var newSections = addFile(section.id, sections);
                                    const newSectionsCopy = [...newSections];
                                    setSections(newSectionsCopy);
                                }}>
                                    <FontAwesomeIcon icon="plus" />&nbsp;Ajouter un fichier
                                </Button>
                            </div>
                        </div>
                    ))}
                </div>

            </Card.Body>
        </Card>
    )
}


// Add a new section to the additional files
function addSection(sections: AdditionalFile[]) {
    sections.push({
        id: sections.length,
        section: "Documents supplémentaires",
        files: []
    });
    return sections;
}

// Add a new file to the additional files
function addFile(sectionId: number, sections: AdditionalFile[]) {
    sections.map((section: AdditionalFile) => {
        if (section.id === sectionId) {
            section.files.push({
                id: section.files.length,
                icon: "",
                title: "",
                description: "",
                buttons: []
            });
        }
    })
    return sections;
}

export default AdditionalFilesEditorContent;