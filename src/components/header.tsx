// ----------------------------
// Header component
// The page header
// ----------------------------

import styles from "./css/header.module.css";

function Header(props: { name: string }) {
  return (
    <h1 className={styles.title} id="pageHeader">{props.name}</h1>
  );
}

export default Header;